[when]Do not Show Test Products {productCodes} to members other than Test members {testMemberIDs} = $bean:DisplayPropBean( programCode == "SGREX" && productCode in ({productCodes}) && memberID not in ({testMemberIDs}))

[then] Do not show the products =  $bean.setIsDisplayable(false);