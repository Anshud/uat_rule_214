[when] When the Member ID belongs to {memberID} blacklist member = $bean:ProgramMemberBean( (programCode == "MMBS"), memberID in ({memberID}))

[when] When the Member status is not Suspended = $bean:ProgramMemberBean( (programCode == "MMBS"), (isCustomer == true), (accountStatus != "Suspended"))

[when] Blacklist all members in MMBS = $bean:ProgramMemberBean((dateToday >= "27-Sep-2017"))

[when] Blacklist everyones in MMBS = $bean:ProgramMemberBean((programCode == "MMBS"))

[then] Blacklist the Members = $bean.setIsBlackListed(true);
[then] Do not blacklist the Members = $bean.setIsBlackListed(false);
[then] Set the Content tag to {contentTag} = $bean.setContentTag(setContentTagData($bean.isBlackListed,"{contentTag}"));

[then] Apply your settings = update($bean);