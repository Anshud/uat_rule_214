[when] For DDREX product visibility for the products {productCodes} and excluding members {memberIDs}  = $bean:DisplayPropBean( productCode in ({productCodes}), memberID not in ({memberIDs}))


[then] Make all products visible =  $bean.setIsDisplayable(true);
[then] Do not show the products =  $bean.setIsDisplayable(false);

[then] Apply your settings = update($bean);