[when] Categories {catCodes} visibility rule in shop for {segmentCodes} segments and not loyagent mode = $bean:DisplayPropBean(programCode == "SCCRREX", categoryCode in ({catCodes}), segmentCode in ({segmentCodes}) , isFromLoyAgent == "false" )

[when] Categories {catCodes} visibility rule in shop for loyagent mode = $bean:DisplayPropBean(programCode == "SCCRREX", categoryCode in ({catCodes}), isFromLoyAgent == "true" )

[when] Hide all redemption types including Categories {catCodes} and including segments {segmentCodes} and not loyagent mode = $bean:DisplayPropBean( categoryCode in ({catCodes}) ,  segmentCode in ({segmentCodes}) , isFromLoyAgent == "false" )



[then] Make categories visible =  $bean.setIsDisplayable(true);
[then] Do not show the categories =  $bean.setIsDisplayable(false);

[then] Apply your settings = update($bean);