[when] For Etihad Channels catalog visibility for the products {channelCodes} = $bean:DisplayPropBean(channelId in ({channelCodes}),eval(checkIfNoADGOVClub($bean,"ADGOV")))

[when] For Etihad Channel product catalog visibility for the March = $bean:DisplayPropBean(eval(checkIfChannelIsVisible($bean)))


[then] Make all products visible =  $bean.setIsDisplayable(true);
[then] Do not show the products =  $bean.setIsDisplayable(false);

[then] Apply your settings = update($bean);