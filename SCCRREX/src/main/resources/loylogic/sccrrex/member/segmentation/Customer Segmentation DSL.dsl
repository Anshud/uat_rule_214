[when] When the Member cardType is {segment} = $bean:ProgramMemberBean( (programCode == "SCCRREX"), (isCustomer == true), (programDefinedSegment in ({segment})))



[then] Set segment code to {segmentCode} = $bean.setSegmentCode("{segmentCode}");

[then] Apply your settings = update($bean);