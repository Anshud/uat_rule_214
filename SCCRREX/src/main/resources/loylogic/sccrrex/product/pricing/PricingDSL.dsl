[when] All products in {programCode} are Points and Cash enabled and not loyagent mode = $bean:MileageAlgoBean(programCode in ("{programCode}") , isFromLoyAgent == "false" ,

[when] Products in {programCode} and buying redemption and non loyagent mode are points plus Cash enabled including products with subredemption type {redemptionSubType} = $bean:MileageAlgoBean(programCode in ("{programCode}") , isFromLoyAgent == "false" , redemptionType in ("B"), redemptionSubType in ({redemptionSubType}))

[when] Products in {programCode} and buying redemption and non loyagent mode are points plus Cash enabled for products with subredemption type {redemptionSubType} excluding Products with codes {productCodes} = $bean:MileageAlgoBean(programCode in ("{programCode}") , isFromLoyAgent == "false" , redemptionType in ("B"), redemptionSubType in ({redemptionSubType}), productCode not in ({productCodes}))

[when] Products in {programCode} and buying redemption and non loyagent mode are points plus Cash enabled excluding products with subredemption type {redemptionSubType} excluding Products with codes {productCodes} = $bean:MileageAlgoBean(programCode in ("{programCode}") , isFromLoyAgent == "false" , redemptionType in ("B"), redemptionSubType not in ({redemptionSubType}), productCode not in ({productCodes}))

[when] Products in {programCode} and buying redemption and non loyagent mode are points plus Cash enabled  = $bean:MileageAlgoBean(programCode in ("{programCode}") , isFromLoyAgent == "false" , redemptionType in ("B"), 

[when] All products in {programCode} and loyagent mode are Points only = $bean:MileageAlgoBean(programCode in ("{programCode}") , isFromLoyAgent == "true")

[when] All products in {programCode} need to be Points-only or Points and Cash enabled = $bean:MileageAlgoBean(programCode in ("{programCode}"), 

[when] Some products in {programCode} need to be Points-only or Points and Cash based on the product codes and not loyagent mode = $bean:MileageAlgoBean(programCode in ("{programCode}"), isFromLoyAgent == "false" ,

[when] Some products in {programCode} need to be Points-only or Points and Cash based on the Merchant codes = $bean:MileageAlgoBean(programCode == "{programCode}", isFromLoyAgent == "false" ,

[when] Some products in {programCode} need to be Points-only or Points and Cash based on the product codes = $bean:MileageAlgoBean(programCode == "{programCode}", isFromLoyAgent == "false" ,

[when] Excluding merchants and products with codes {productCodes} = productCode not in ({productCodes}), merchantCode not in ("MER2865","MER2983","MER2992"))

[when] Excluding Products with codes {productCodes} = productCode not in ({productCodes}))
[when] Including Products with codes {productCodes} = productCode in ({productCodes}))

[when] Excluding Products from Merchants with codes {merchantCodes} = merchantCode not in ({merchantCodes}))
[when] Including Products from Merchants with codes {merchantCodes} = merchantCode in ({merchantCodes}))



[then] Set the Minimum miles % to {minMilePercent}= $bean.setPercentMinMiles({minMilePercent});
[then] Set the Minimum miles to {minMiles}= $bean.setMinMiles({minMiles});

[then] Set the Maximum miles % to {maxMilePercent}= $bean.setPercentMaxMiles({maxMilePercent});
[then] Set the Maximum miles to {maxMiles}= $bean.setMaxMiles({maxMiles});

[then] Set the Slider Rounding type to {roundingType} = $bean.setSliderRoundingType("{roundingType}");
[then] Set the Slider Rounding unit to {roundingUnit} = $bean.setSliderRoundingUnit({roundingUnit});

[then] Apply your settings = update($bean);