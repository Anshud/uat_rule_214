[when]Do not Show Test Products {productCodes} to members other than Test members {testMemberIDs} = $bean:DisplayPropBean( productCode in ({productCodes}) && memberID not in ({testMemberIDs}) && isDisplayable == "true")

[then] Do not show the products =  $bean.setIsDisplayable(false);