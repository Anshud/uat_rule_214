[when] All products in {programCode} = $bean : MileageAlgoBean(programCode in ("{programCode}"))

[when] All non-voucher buying products in {programCode} need a special CPPL = $bean : MileageAlgoBean(programCode == ("{programCode}"), redemptionType == ("B"), redemptionSubType not in ("CF","SC","MB","IW","GC","EX","EV","DC","AW","AL"))

[when] Exclusive buying products in {programCode} need a special CPPL = $bean : MileageAlgoBean(programCode == ("{programCode}"), redemptionType == ("B"), redemptionSubType not in ("CF","SC","MB","IW","GC","EX","EV","DC","AW","AL"),

[when] Voucher, gift card products in {programCode} need a special CPPL = $bean : MileageAlgoBean(programCode == ("{programCode}"), redemptionType == ("B"), redemptionSubType in ("GC","EV","EX","MB"))

[when] Statement credit products in {programCode} need a special CPPL = $bean : MileageAlgoBean(programCode == ("{programCode}"), redemptionType == ("B"), redemptionSubType == ("SC"))

[when] Annual credit card fee in {programCode} need a special CPPL with codes {productCodes} = $bean : MileageAlgoBean(programCode == ("{programCode}"), redemptionType == ("B"), redemptionSubType == ("CF"), productCode in ({productCodes}) )

[when] For selective products in {programCode} need a special CPPL = $bean : MileageAlgoBean(programCode == ("{programCode}"), redemptionType == ("B"),

[when] Donations products in {programCode} need a special CPPL = $bean : MileageAlgoBean(programCode == ("{programCode}"), redemptionType == ("G") )

[when] Points transfer products in {programCode} need a special CPPL = $bean : MileageAlgoBean(programCode == ("{programCode}"), redemptionType == ("P") )


[when] Some products in {programCode} need a special CPPL based on the product codes = $bean : MileageAlgoBean(programCode == ("{programCode}"),
 
[when] Including Products with codes {productCodes} = productCode in ({productCodes}))
[when] Excluding Products with codes {productCodes} = productCode not in ({productCodes}))


[then] Set the CPPL Offset % to {cpplOffsetPercent}= $bean.setCpplOffsetPercent({cpplOffsetPercent});
[then] Set the CPPL Offset to {cpplOffset}= $bean.setCpplOffset({cpplOffset});

[then] Set the Audit comments to {commentsForAudit} = $bean.setCommentsForAudit("{commentsForAudit}");

[then] Apply your settings = update($bean);