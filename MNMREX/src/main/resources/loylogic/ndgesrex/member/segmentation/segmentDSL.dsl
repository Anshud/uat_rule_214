[when] When the Member status is Suspended or Inactive = $bean:ProgramMemberBean( programCode in ("MNMREX"), accountStatus in ("Suspended" , "Inactive"))

[when] When the Member status is not Suspended = $bean:ProgramMemberBean( (programCode == "MNMREX"), (accountStatus != "Suspended"))

[when] When program is {programID} member is blacklisted {memberid} = $bean:ProgramMemberBean( (programCode in ({programID})),memberID in ({memberid}))

[when] When program member is {programID} below 18 years blacklisted {age} = $bean:ProgramMemberBean( (programCode in ({programID})),age <= ({age}))

[when] When current Date is between 2 Aug and 10 Aug for Etihad = $bean:ProgramMemberBean( (programCode == "MNMREX"), (dateToday >= "21-Mar-2019"), (dateToday <= "30-May-2019"))

[when] When program member is {programID} below 18 years blacklisted {age} = $bean:ProgramMemberBean( (programCode in ({programID})),age <= ({age}))

[then] Set the promotional Content tag Max to {contentTag} = $bean.setContentTagMaximized("{contentTag}");
[then] Set the promotional Content tag Min to {contentTagMin} = $bean.setContentTagMinimized("{contentTagMin}");

[then] Blacklist the Members = $bean.setIsBlackListed(true);
[then] Do not blacklist the Members = $bean.setIsBlackListed(false);
[then] Set the segmentCode of member {segmentCode} = $bean.setSegmentCode({segmentCode});
[then] Set the Content tag to {contentTag} = $bean.setContentTag(setContentTagData($bean.isBlackListed,"{contentTag}"));

[then] Apply your settings = update($bean);