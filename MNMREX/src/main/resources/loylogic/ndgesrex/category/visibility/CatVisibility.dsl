
[when] Category visibility rule MNMREX = $bean:DisplayPropBean(programCode == "MNMREX", categoryCode in ("SHPCAT170"),(isCustomer == false))

[then] Make categories visible =  $bean.setIsDisplayable(true);
[then] Do not show the categories =  $bean.setIsDisplayable(false);

[then] Apply your settings = update($bean);