[when]Program code condition validation = $bean:BuyGiftPointsBean(programCode == "MNMREX",
[when]Module condition validation {module} = module == {module},
[when]Promotion code empty validation = promoCode != null,
[when]Promotion code condition validation {promoCodes} = eval(checkPromoCodeValidity($bean, ({promoCodes})))
)

[then] Set promotion cash {cashAmount} = $bean.setCashAmount({cashAmount});
[then] Set promotion points {points} = $bean.setPoints({points});
[then] Set promotion starts on {startDate} and expires on {endDate}= checkPromoExpiry($bean,{startDate},{endDate});
[then] Set promotion validation true = $bean.setValidPromoCode(true);
[then] Set promotion currency {currencyISOCode} = $bean.setCurrencyISOCode({currencyISOCode});
[then] Set promotion type {promoType} = $bean.setPromoType({promoType});
[then] Set limit per member value {limitPerMember} = $bean.setLimitPerMember({limitPerMember});
[then] Set order limit value {orderLimit} = $bean.setOrderLimit({orderLimit});
[then] Set promotion Client Percent {promotionClientPercent} = $bean.setPromotionClientPercent({promotionClientPercent});
[then] Set promotion LL Percent {promotionLLPercent} = $bean.setPromotionLLPercent({promotionLLPercent});
