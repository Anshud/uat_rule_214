[when] Categories {catCodes} visibility rule in shop for ADGOV = $bean:DisplayPropBean(programCode == "ETH", categoryCode in ({catCodes}), eval(checkIfNoADGOVClub($bean,"ADGOV")))

[when] Categories {catCodes} visibility rule in shop for {tiers} tiers = $bean:DisplayPropBean(programCode == "ETH", categoryCode in ({catCodes}), eval(checkIfNoTier($bean,{tiers})))

[when] Category visibility rule in shop = $bean:DisplayPropBean(programCode == "ETH", categoryCode in ("SHPCAT229"), eval(checkIfNotABirthDayWeek($bean)))

[when] Category visibility rule for Ramdan = $bean:DisplayPropBean(programCode == "ETH", categoryCode not in ("SHPCAT750", "SHPCAT551", "SHPCAT749"), eval(checkIfCategoryIsVisible($bean)))

[when] When Category code is {categoryCode} and member IDs are not {memberIDs}= $bean:DisplayPropBean(programCode == "ETH", categoryCode in ({categoryCode}), memberID not in ({memberIDs}))

[when] When Visitor mode  and Categories in {catCodes} and basket country in {countryCodes} = $bean:DisplayPropBean( (programCode == "ETH"), (isCustomer == false) , categoryCode in ({catCodes}) , countryInCart in ({countryCodes}))

[when] When Member Logged in and has no active reward card and Categories in {catCodes} and basket country in {countryCodes} = $bean:DisplayPropBean( (programCode == "ETH"), (isCustomer == true) , (userHasActiveRewardCard == false) , categoryCode in ({catCodes}) ,countryInCart in ({countryCodes}))


[when] When programCode is ETH and categoryCode is {category} = $bean:DisplayPropBean( (programCode == "ETH"), categoryCode in ({category}))




[then] Make categories visible =  $bean.setIsDisplayable(true);
[then] Do not show the categories =  $bean.setIsDisplayable(false);

[then] Apply your settings = update($bean);