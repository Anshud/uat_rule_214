[when] All products in {programCode} need to be Points-only or Points and Cash enabled = $bean:MileageAlgoBean(programCode == "{programCode}", 

[when] Some products in {programCode} need to be Points-only or Points and Cash based on the Merchant codes = $bean:MileageAlgoBean(programCode == "{programCode}", 

[when] Some products in {programCode} need to be Points-only or Points and Cash based on the product codes = $bean:MileageAlgoBean(programCode == "{programCode}", 

[when] Excluding Products with codes {productCodes} = productCode not in ({productCodes}))
[when] Including Products with codes {productCodes} = productCode in ({productCodes}))

[when] Excluding Products from Merchants with codes {merchantCodes} = merchantCode not in ({merchantCodes}))
[when] Including Products from Merchants with codes {merchantCodes} = merchantCode in ({merchantCodes}))

[when] Excluding Brands with codes {brandCodes} = brandCode not in ({brandCodes}),
[when] Including Brandswith codes {brandCodes} = brandCode in ({brandCodes}))

[then] Set the Minimum miles % to {minMilePercent}= $bean.setPercentMinMiles({minMilePercent});
[then] Set the Minimum miles to {minMiles}= $bean.setMinMiles({minMiles});

[then] Set the Maximum miles % to {maxMilePercent}= $bean.setPercentMaxMiles({maxMilePercent});
[then] Set the Maximum miles to {maxMiles}= $bean.setMaxMiles({maxMiles});

[then] Set the Slider Rounding type to {roundingType} = $bean.setSliderRoundingType("{roundingType}");
[then] Set the Slider Rounding unit to {roundingUnit} = $bean.setSliderRoundingUnit({roundingUnit});

[then] Apply your settings = update($bean);