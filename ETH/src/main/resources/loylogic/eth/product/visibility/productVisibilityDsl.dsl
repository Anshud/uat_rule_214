[when]Do not Show Test Products {productCodes} to members other than Test members {testMemberIDs} = $bean:DisplayPropBean( programCode == "ETH" && productCode in ({productCodes}) && memberID not in ({testMemberIDs}))

[when] For Etihad Birthday Channel product catalog visibility for the products {productCodes} = $bean:DisplayPropBean(redemptionType == "B", productCode in ({productCodes}), eval(checkIfNotABirthDayWeek($bean)))

[when] For Etihad Channels catalog visibility for the products {prdCodes} = $bean:DisplayPropBean(redemptionType == "B", productCode in ({prdCodes}),eval(checkIfNoADGOVClub($bean,"ADGOV")))

[when] Etihad Channels catalog visibility for the products {prdCodes} for tiers present in {tiers}= $bean:DisplayPropBean(productCode in ({prdCodes}),eval(checkIfNoTier($bean,{tiers})))

[then] Make all products visible =  $bean.setIsDisplayable(true);
[then] Do not show the products =  $bean.setIsDisplayable(false);

[then] Apply your settings = update($bean);