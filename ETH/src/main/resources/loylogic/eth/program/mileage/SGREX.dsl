[when] All products in {programCode} and redmption Types in {redemptionTypes} from 19 March 2017 = $bean : MileageAlgoBean(programCode == "{programCode}",redemptionType in ({redemptionTypes}), (dateToday >= "19-Mar-2017"),

[when] All products in {programCode} and redmption Types in {redemptionTypes} before 19 March 2017 = $bean : MileageAlgoBean(programCode == "{programCode}",redemptionType in ({redemptionTypes}), (dateToday < "19-Mar-2017"),

[when] Some products in {programCode} need a special CPPM based on the product codes = $bean : MileageAlgoBean(programCode == "{programCode}", 

[when] Some products in {programCode} need a special CPPM based on the product codes before 19 March 2017 = $bean : MileageAlgoBean(programCode == "{programCode}", (dateToday < "19-Mar-2017"),

[when] Including Products with codes {productCodes} = productCode in ({productCodes}))
[when] Excluding Products with codes {productCodes} = productCode not in ({productCodes}))

[when] Including Products from Merchants with codes {merchantCodes} = merchantCode in ({merchantCodes}))
[when] Excluding Products from Merchants with codes {merchantCodes} = merchantCode not in ({merchantCodes}))


[then] Set the Algorithm code to {algorithmCode} = $bean.setAlgorithmCode("{algorithmCode}");
[then] Set the CPPM to {conversionRatio} = $bean.setMilesConversionRatio({conversionRatio});      
[then] Set the oldCPPM to {oldConversionRatio} = $bean.setMilesConversionRatioStrikeThrough({oldConversionRatio}); 
[then] Set the Rounding type to {roundingType} = $bean.setMilesRoundingType("{roundingType}");
[then] Set the Rounding unit to {roundingUnit} = $bean.setMilesRoundingUnit({roundingUnit});
[then] Set the Floor price to {floorPrice} = $bean.setFloorPrice({floorPrice});
[then] Set the Audit comments to {commentsForAudit} = $bean.setCommentsForAudit("{commentsForAudit}");
[then] Set the Brokerage Commission % to {brokrageCommissionProducts}= $bean.setBrokrageCommissionProducts({brokrageCommissionProducts});
[then] set the Loymart Sales Price Contribution Share % to {loymartSalesPriceContributionShare}= $bean.setLoymartSalesPriceContributionShare({loymartSalesPriceContributionShare});
[then] Set the Pricing Tier Code to {pricingTierCodes} = $bean.setPricingTierCode("{pricingTierCodes}");
[then] Set the Correction Factor value to {correctionValue} = $bean.setCorrectionFactor({correctionValue});

[then] logg = System.out.println("------- THIS IS SYS OUT FROM RULES -----------");
[then] Apply your settings = update($bean);