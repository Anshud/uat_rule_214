[when] For Etihad Channels catalog visibility for the products {channelCodes} = $bean:DisplayPropBean(channelId in ({channelCodes}),eval(checkIfNoADGOVClub($bean,"ADGOV")))

[when] For Etihad Channel product catalog visibility for the March = $bean:DisplayPropBean(eval(checkIfChannelIsVisible($bean)))


[then] Make all products visible =  $bean.setIsDisplayable(true);
[then] Do not show the products =  $bean.setIsDisplayable(false);

[then] Apply your settings = update($bean);

[when] Etihad Primary Key Visuals {keyVisualIds} visibility rule for the countries {countryCodes} = $bean:DisplayPropBean(keyVisualId in ({keyVisualIds}), countryInCart not in ({countryCodes}))

[when] Etihad Primary Key Visuals {keyVisualIds} visibility rule for excluding the countries {countryCodes} = $bean:DisplayPropBean(keyVisualId in ({keyVisualIds}), countryInCart in ({countryCodes}))

[when] Etihad Banners {banIds} visibility rule for the countries {countryCodes} = $bean:DisplayPropBean(bannerId in ({banIds}), countryInCart in ({countryCodes}))

[when] Etihad Primary Key Visuals {pkvs} visibility rule for the Europe region = $bean:DisplayPropBean(keyVisualId in ({pkvs}), countryInCart not in ("by","fo","lt","si","al","ad","cz","gi","is","rs","es","ch","be","ee","fr","it","lu","va","hr","cy","fi","gr","md","dk","hu","ie","xx","lv","mc","no","pt","se","me","at","ba","bg","de","li","mk","mt","ro","ru","gb","pl","sm","ua","nl","sk"))

[when] Etihad Primary Key Visuals {pkvs1} visibility rule excluding Europe region = $bean:DisplayPropBean(keyVisualId in ({pkvs1}), countryInCart in ("by","fo","lt","si","al","ad","cz","gi","is","rs","es","ch","be","ee","fr","it","lu","va","hr","cy","fi","gr","md","dk","hu","ie","xx","lv","mc","no","pt","se","me","at","ba","bg","de","li","mk","mt","ro","ru","gb","pl","sm","ua","nl","sk"))

[when] Etihad Primary Key Visuals {pkvs2} visibility rule for Europe and GCC regions = $bean:DisplayPropBean(keyVisualId in ({pkvs2}), countryInCart not in ("by","fo","lt","si","al","ad","cz","gi","is","rs","es","ch","be","ee","fr","it","lu","va","hr","cy","fi","gr","md","dk","hu","ie","xx","lv","mc","no","pt","se","me","at","ba","bg","de","li","mk","mt","ro","ru","gb","pl","sm","ua","nl","sk","ae","bh","kw","om","qa","sa"))

[when] Etihad Primary Key Visuals {pkvs3} visibility rule for excluding Europe and GCC regions = $bean:DisplayPropBean(keyVisualId in ({pkvs3}), countryInCart in ("by","fo","lt","si","al","ad","cz","gi","is","rs","es","ch","be","ee","fr","it","lu","va","hr","cy","fi","gr","md","dk","hu","ie","xx","lv","mc","no","pt","se","me","at","ba","bg","de","li","mk","mt","ro","ru","gb","pl","sm","ua","nl","sk","ae","bh","kw","om","qa","sa"))


[then] Make all products visible =  $bean.setIsDisplayable(true);
[then] Do not show the products =  $bean.setIsDisplayable(false);

[then] Apply your settings = update($bean);