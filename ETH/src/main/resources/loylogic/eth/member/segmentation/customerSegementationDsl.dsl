[when] When the Member status is Suspended = $bean:ProgramMemberBean( (programCode == "ETH"), (isCustomer == true), (accountStatus == "Suspended"))
[when] When the Member 729800000871 is to be Suspended = $bean:ProgramMemberBean( (programCode == "ETH"), (isCustomer == true), (memberID == "729800000871"))

[when] When the Member status is not Suspended = $bean:ProgramMemberBean( (programCode == "ETH"), (isCustomer == true), (accountStatus != "Suspended"))
[when] When current Date is between 2 Aug and 10 Aug for Etihad = $bean:ProgramMemberBean( (programCode == "ETH"), (dateToday >= "02-Aug-2017"), (dateToday <= "30-Dec-2018"))
[when] if member is Loyagent = $bean:ProgramMemberBean( (programCode == "ETH"), isFromLoyAgent == true);


[then] Set segment code to {segmentCode} = $bean.setSegmentCode("{segmentCode}");
[then] Blacklist the Members = $bean.setIsBlackListed(true);
[then] Do not blacklist the Members = $bean.setIsBlackListed(false);
[then] Set the Content tag to {contentTag} = $bean.setContentTag(setContentTagData($bean.isBlackListed,"{contentTag}"));
[then] Set the promotional Content tag Max to {contentTag} = $bean.setContentTagMaximized("{contentTag}");
[then] Set the promotional Content tag Min to {contentTagMin} = $bean.setContentTagMinimized("{contentTagMin}");
[then] Apply your settings = update($bean);