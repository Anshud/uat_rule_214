[when] When Visitor mode  and merchantCode in {merchantCodes} and basket country in {countryCodes} = $bean:DisplayPropBean( (programCode == "EYBM"), (isCustomer == false) , merchantCode in ({merchantCodes}) , countryInCart in ({countryCodes}))

[when] When Member Logged in and has no active reward card  and merchantCode in {merchantCodes}  and basket country in {countryCodes} = $bean:DisplayPropBean( (programCode == "EYBM"), (isCustomer == true) , (userHasActiveRewardCard == false) , merchantCode in ({merchantCodes}), countryInCart in ({countryCodes}))


[then] Do not show the merchant =  $bean.setIsDisplayable(false);
[then] set temp message as {warningIndicator} = $bean.setWarningIndicator("{warningIndicator}")

[then] Apply your settings = update($bean);