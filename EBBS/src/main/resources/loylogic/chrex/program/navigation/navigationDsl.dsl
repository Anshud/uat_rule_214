[when] The segment code is VISITOR show login pop up for {programCodes} = $bean:DisplayPropBean(programCode == {programCodes}, (segmentCode in ("021_VST","022_VST","023_VST") && applicationUrl not in ("joinnow_EN","joinnow_FR_FR","homeIconAltText_EN","clientFooterLogoURL_EN","clientFooterLogoURL_FR_FR")))

[then] Show login pop up = $bean.setShowLoginPopup(true);
[then] Apply your settings = update($bean);
[then] Set Application URL = $bean.setApplicationUrl("\#");