[when] For 021 Segment and GOLD1 Tier members products visibility rule for the products {productCodes} = $bean:DisplayPropBean( programCode == "EBBS", productCode in ({productCodes}), segmentCode not in ("021_GOLD1","021_CRD_KCLM","021_CRD_KCLS","021_CRD_KSLM","021_CRD_KSLS"))
[when] For 021 Segment and PLAT1 Tier members products visibility rule for the products {productCodes} = $bean:DisplayPropBean( programCode == "EBBS", productCode in ({productCodes}), segmentCode not in ("021_PLAT1"))
[when] For 021 Segment and GREEN1 Tier members products visibility rule for the products {productCodes} = $bean:DisplayPropBean( programCode == "EBBS", productCode in ({productCodes}), segmentCode not in ("021_GREEN1","021_CRD_KCGM","021_CRD_KCGS","021_CRD_KSGM","021_CRD_KSGS"))

[when] For 022 Segment and GOLD3 Tier members products visibility rule for the products {productCodes} = $bean:DisplayPropBean( programCode == "EBBS", productCode in ({productCodes}), segmentCode not in ("022_GOLD3","022_CRD_UCLM","022_CRD_UCLS","022_CRD_USLM","022_CRD_USLS"))
[when] For 022 Segment and PLAT3 Tier members products visibility rule for the products {productCodes} = $bean:DisplayPropBean( programCode == "EBBS", productCode in ({productCodes}), segmentCode not in ("022_PLAT3"))
[when] For 022 Segment and GREEN3 Tier members products visibility rule for the products {productCodes} = $bean:DisplayPropBean( programCode == "EBBS", productCode in ({productCodes}), segmentCode not in ("022_GREEN3","022_CRD_UCGM","022_CRD_UCGS","022_CRD_USGM","022_CRD_USGS"))

[when] For 023 Segment and GOLD2 Tier members products visibility rule for the products {productCodes} = $bean:DisplayPropBean( programCode == "EBBS", productCode in ({productCodes}), segmentCode not in ("023_GOLD2","023_CRD_TCLM","023_CRD_TCLS","023_CRD_TSLM","023_CRD_TSLS"))
[when] For 023 Segment and PLAT2 Tier members products visibility rule for the products {productCodes} = $bean:DisplayPropBean( programCode == "EBBS", productCode in ({productCodes}), segmentCode not in ("023_PLAT2"))
[when] For 023 Segment and GREEN2 Tier members products visibility rule for the products {productCodes} = $bean:DisplayPropBean( programCode == "EBBS", productCode in ({productCodes}), segmentCode not in ("023_GREEN2","023_CRD_TCGM","023_CRD_TCGS","023_CRD_TSGM","023_CRD_TSGS"))


[then] Make all products visible =  $bean.setIsDisplayable(true);
[then] Do not show the products =  $bean.setIsDisplayable(false);

[then] Apply your settings =  update($bean);