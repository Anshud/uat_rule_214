[when] When the 021 segment Member as visitor with tier is null = $bean:ProgramMemberBean(programCode == "EBBS", segmentCode in ("021"), tierlevel in (null,"null"))

[when] When the 022 segment Member as visitor with tier is null = $bean:ProgramMemberBean(programCode == "EBBS", segmentCode in ("022"), tierlevel in (null,"null"))

[when] When the 023 segment Member as visitor with tier is null = $bean:ProgramMemberBean(programCode == "EBBS", segmentCode in ("023"), tierlevel in (null,"null"))

[when] When Test member = $bean:ProgramMemberBean(programCode == "EBBS", memberID in ("TEST342","0222000026000", "EQ6000" ))


[then] Set the segment code as Visitor {vstCode} = $bean.setSegmentCode({vstCode});
[then] Blacklist the Members = $bean.setIsBlackListed(true);
[then] Do not blacklist the Members = $bean.setIsBlackListed(false);
[then] Set the Content tag to {contentTag} = $bean.setContentTag(setContentTagData($bean.isBlackListed,"{contentTag}"));

[then] Apply your settings = update($bean);