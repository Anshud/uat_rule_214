[when] Categories {catCodes} visibility rule in shop for {segmentCodes} segments = $bean:DisplayPropBean(programCode in ("SCMRREX"), categoryCode in ({catCodes}), segmentCode in ({segmentCodes}) )

[when] Categories {catCodes} visibility rule in shop excluding {segmentCodes} segments = $bean:DisplayPropBean(programCode in ("SCMRREX"), categoryCode in ({catCodes}), segmentCode not in ({segmentCodes}) )

[when] Categories {catCodes} visibility rule in shop for loyagent mode = $bean:DisplayPropBean(programCode in ("SCMRREX"), categoryCode in ({catCodes}), isFromLoyAgent == "true" )

[when] Hide all Categories {catCodes} and excluding segments {segmentCodes} and is Displayable = $bean:DisplayPropBean( categoryCode in ({catCodes}) ,  segmentCode not in ({segmentCodes}) , isDisplayable == "true" )



[then] Make categories visible =  $bean.setIsDisplayable(true);
[then] Do not show the categories =  $bean.setIsDisplayable(false);

[then] Apply your settings = update($bean);