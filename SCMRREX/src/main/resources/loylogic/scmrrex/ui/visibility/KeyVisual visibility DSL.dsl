[when] Primary Key Visuals {keyVisualIds} visibility rule for the segmentCodes {segmentCodes} = $bean:DisplayPropBean(keyVisualId in ({keyVisualIds}) , segmentCode in ({segmentCodes}) )

[when] Primary Key Visuals {keyVisualIds} visibility rule for loyagent mode = $bean:DisplayPropBean(keyVisualId in ({keyVisualIds}) , isFromLoyAgent == "true" )

[when] Primary Key Visuals {keyVisualIds} visibility rule excluding the segmentCodes {segmentCodes}  = $bean:DisplayPropBean(keyVisualId in ({keyVisualIds}) , segmentCode not in ({segmentCodes}) )




[then] Make all products visible =  $bean.setIsDisplayable(true);
[then] Do not show the products =  $bean.setIsDisplayable(false);

[then] Apply your settings = update($bean);