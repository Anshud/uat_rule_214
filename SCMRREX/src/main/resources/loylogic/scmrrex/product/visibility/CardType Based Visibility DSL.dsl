[when] For SCMRREX product visibility based on member's Card Type = $bean:DisplayPropBean(redemptionSubType in ("CF"),redemptionType in ("B"), isDisplayable == "true", eval(checkCardBasedProductVisibility($bean)))



[then] Make all products visible =  $bean.setIsDisplayable(true);
[then] Do not show the products =  $bean.setIsDisplayable(false);


[then] Apply your settings = update($bean);