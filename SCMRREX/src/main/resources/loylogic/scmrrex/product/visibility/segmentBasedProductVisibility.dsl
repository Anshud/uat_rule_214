[when] When products are in {prdCodes} and memberID not in {memberID} do not show the products = $bean:DisplayPropBean(productCode in ({prdCodes}),memberID not in ({memberID}))

[when] For SCMRREX product visibility for redemptionSubType excluding {redemptionSubType} and redemptionType excluding {redemptionType} and including segments {segmentCodes} = $bean:DisplayPropBean( redemptionSubType not in ({redemptionSubType}), redemptionType not in ({redemptionType}) ,  segmentCode in ({segmentCodes}))

[when] Hide all redemptionSubType excluding {redemptionSubType} and including segments {segmentCodes} = $bean:DisplayPropBean( redemptionSubType not in ({redemptionSubType}), redemptionType in ("B") ,  segmentCode in ({segmentCodes}) , isDisplayable == "true")

[when] Hide all redemptionSubType inclucing {redemptionSubType} and exluding segments {segmentCodes} = $bean:DisplayPropBean( redemptionSubType in ({redemptionSubType}), redemptionType in ("B") ,  segmentCode not in ({segmentCodes}) , isDisplayable == "true")

[when] Hide all redemption types excluding {redemptionType} and including segments {segmentCodes} = $bean:DisplayPropBean( redemptionType not in ({redemptionType}) ,  segmentCode in ({segmentCodes}) , isDisplayable == "true")

[when] For SCMRREX product visibility for redemptionSubType including {redemptionSubType} and redemptionType including {redemptionType} and including segments {segmentCodes}  = $bean:DisplayPropBean( redemptionSubType in ({redemptionSubType}), redemptionType in ({redemptionType}) ,  segmentCode in ({segmentCodes}))

[when] Hide all redemption types including {redemptionType} and including segments {segmentCodes} = $bean:DisplayPropBean( redemptionType in ({redemptionType}) ,  segmentCode in ({segmentCodes}) )


[when] Hide redemption types including {redemptionType} for excluding Tier members with segments {segmentCodes} = $bean:DisplayPropBean( redemptionType in ({redemptionType}) ,  (segmentCode not in ({segmentCodes})) , isDisplayable == "true")


[then] Make all products visible =  $bean.setIsDisplayable(true);
[then] Do not show the products =  $bean.setIsDisplayable(false);
[then] set temp message as {warningIndicator} = $bean.setWarningIndicator("{warningIndicator}")

[then] Apply your settings = update($bean);