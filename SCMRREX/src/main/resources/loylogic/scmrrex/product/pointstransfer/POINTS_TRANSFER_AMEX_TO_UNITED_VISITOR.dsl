[when] The member wants to transfer MR Points to {transferID} = $bean : MileageAlgoBean(partnerTransferId =="{transferID}")

[when] When current date is greater than equal {startDate} and member wants to transfer MR Points to {transferID}   = $bean : MileageAlgoBean(partnerTransferId =="{transferID}", dateToday >= "{startDate}")

[when] When current date is Less than equal {startDate} and member wants to transfer MR Points to {transferID}   = $bean : MileageAlgoBean(partnerTransferId =="{transferID}", dateToday < "{startDate}")

[then] Set the conversion ratio to {pointsTransferconversionRatio}:{pointsTransferconversionRatio1} = $bean.setPointsTransferconversionRatio("{pointsTransferconversionRatio}:{pointsTransferconversionRatio1}");

[then] Apply your settings = update($bean);