[when] When the Member cardType is {segment} = $bean:ProgramMemberBean( (programCode == "SCMRREX"), (isCustomer == true), (programDefinedSegment in ({segment})))

[when] When the Member tier level is {tier} = $bean:ProgramMemberBean( (programCode == "SCMRREX"), (isCustomer == true), (tierlevel in ({tier})))

[when] When Loyagent is logged in with tier {tier} = $bean:ProgramMemberBean( (programCode == "SCMRREX"), isFromLoyAgent == true, (tierlevel in ({tier})));


[then] Set segment code to {segmentCode} = $bean.setSegmentCode("{segmentCode}");

[then] Apply your settings = update($bean);