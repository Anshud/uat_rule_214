[when] if member is corporate customer = $bean:ProgramMemberBean( (programCode == "SCMRREX"), corporateIndicator == true);

[when] if member is Loyagent = $bean:ProgramMemberBean( (programCode == "SCMRREX"), isFromLoyAgent == true);

[then] Set segment code to {segmentCode} = $bean.setSegmentCode("{segmentCode}");

[then] Apply your settings = update($bean);


