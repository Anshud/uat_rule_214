[when] Hide all merchants including segments {segmentCodes} = $bean:DisplayPropBean( segmentCode in ({segmentCodes}) )

[then] Do not show the products =  $bean.setIsDisplayable(false);
[then] set temp message as {warningIndicator} = $bean.setWarningIndicator("{warningIndicator}")

[then] Apply your settings = update($bean);