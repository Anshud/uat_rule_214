[when] All products in {programCode} and redmptionType in {redemptionTypes} = $bean : MileageAlgoBean((programCode == "{programCode}"), redemptionType in ({redemptionTypes}))  

[when] Some products in {programCode} need a special CPPM based on the product codes = $bean : MileageAlgoBean(programCode == "{programCode}",

[when] Including Products with codes {productCodes} = productCode in ({productCodes}))
[when] Excluding Products with codes {productCodes} = productCode not in ({productCodes}))

[when] Including Products from Merchants with codes {merchantCodes} = merchantCode in ({merchantCodes}))
[when] Excluding Products from Merchants with codes {merchantCodes} = merchantCode not in ({merchantCodes}))


[then] Set the Algorithm code to {algorithmCode} = $bean.setAlgorithmCode("{algorithmCode}");
[then] Set the CPPM to {conversionRatio} = $bean.setMilesConversionRatio(new Double({conversionRatio}));              
[then] Set the Rounding type to {roundingType} = $bean.setMilesRoundingType("{roundingType}");
[then] Set the Rounding unit to {roundingUnit} = $bean.setMilesRoundingUnit({roundingUnit});
[then] Set the Floor price to {floorPrice} = $bean.setFloorPrice(new Double({floorPrice}));
[then] Set the Ceil price to {ceilPrice} = $bean.setCeilingPrice(new Double({ceilPrice}));
[then] Set the Audit comments to {commentsForAudit} = $bean.setCommentsForAudit("{commentsForAudit}");
[then] Set the Brokerage Commission % to {brokrageCommissionProducts}= $bean.setBrokrageCommissionProducts(new Float({brokrageCommissionProducts}));
[then] set the Loymart Sales Price Contribution Share % to {loymartSalesPriceContributionShare}= $bean.setLoymartSalesPriceContributionShare(new Float({loymartSalesPriceContributionShare}));
[then] Set the Pricing Tier Code to {pricingTierCodes} = $bean.setPricingTierCode("{pricingTierCodes}");
[then] Set the Correction Factor value to {correctionValue} = $bean.setCorrectionFactor(new Double({correctionValue}));

[then] Apply your settings = update($bean);