[when] When the Member status is Suspended or Inactive = $bean:ProgramMemberBean( programCode in ("JMREX"), accountStatus in ("Suspended" , "Inactive"))
[when] When the Member status is not Suspended = $bean:ProgramMemberBean( (programCode == "JMREX"), (accountStatus != "Suspended"))
[when] When current Date is between 2 Aug and 30 Oct for Choice = $bean:ProgramMemberBean( (programCode == "JMREX"), (dateToday >= "02-Aug-2017"), (dateToday <= "30-Oct-2018"))
[when] When program is {programID} member is blacklisted {memberid} = $bean:ProgramMemberBean( (programCode in ({programID})),memberID in ({memberid}))





[then] Blacklist the Members = $bean.setIsBlackListed(true);
[then] Do not blacklist the Members = $bean.setIsBlackListed(false);
[then] Set the Content tag to {contentTag} = $bean.setContentTag(setContentTagData($bean.isBlackListed,"{contentTag}"));
[then] Set the promotional Content tag Max to {contentTagMax} = $bean.setContentTagMaximized("{contentTagMax}");
[then] Set the promotional Content tag Min to {contentTagMin} = $bean.setContentTagMinimized("{contentTagMin}");
[then] Apply your settings = update($bean);