

[when] When the program ID {programs} is and coupon ID is {coupon} = $bean:CouponsManagementBean(programCode in ({programs}), couponID in ({coupon}))


[then] Apply coupon to Entire shop = $bean.setCouponApplicableOnEntireShop(true)
[then] Set the discount to {discount} = $bean.setDiscount(({discount}))
[then] Set the Min Shopping Amount to {minShpAmt} = $bean.setMinShoppingAmount(({minShpAmt}))
[then] Set the Max Shopping Discount to {discount} = $bean.setMaxShoppingDiscount(({discount}))
[then] Set the coupon currency to {currency} = $bean.setCouponCurrency((selectCurrency({currency})))
[then] Set the coupon description to {description} = $bean.setCouponDescription({description})
[then] Set the Sponsorer to {sponsor} = $bean.setSponser({sponsor})
[then] Set the sponsor Typeto {sponsertype} = $bean.setSponserType({sponsertype})
[then] Set the coupon as valid = $bean.setIsCouponValid(true)
[then] Set the product list for which coupons is applicable {products} = $bean.setProductList((makeProductList({products})))
[then] Set the Merchant list for which coupons is applicable {merchants} = $bean.setMerchantList((makeMerchantList({merchants})))
[then] Set the Brands list for which coupons is applicable {brands} = $bean.setBrandList((makeBrandList({merchants})))
[then] Set the Discount type of coupon as PERCENTAGE_DISCOUNT_COUPON = $bean.setDiscountType("PERCENTAGE_DISCOUNT_COUPON")
[then] Set the Discount type of coupon as CASH_DISCOUNT_COUPON = $bean.setDiscountType("ABSOLUTE_CASH_DISCOUNT_COUPON")
[then] Set the Discount type of coupon as POINTS_DISCOUNT_COUPON = $bean.setDiscountType("ABSOLUTE_POINTS_DISCOUNT_COUPON")
[then] Set the Discount type of coupon as FREE_SHIPPING_DISCOUNT_COUPON = $bean.setDiscountType("FREE_SHIPPING_DISCOUNT_COUPON")
[then] Set Coupon Validity Start Date {validityStartDate} = $bean.setCouponValidityStartDate({validityStartDate})
[then] Set Coupon Expiry Date {expiryDate} = $bean.setCouponExpiryDate({expiryDate})
[then] Set Coupon Usage Limit Per Member {usageLimitPerMember} = $bean.setUsageLimitPerMember(new Integer({usageLimitPerMember}))
[then] Set the product list for which coupon should not be applied {products} = $bean.setProductListToExclude((makeProductList({products})))
[then] Set the Merchant list for which coupons should not be applied {merchants} = $bean.setMerchantListToExclude((makeMerchantList({merchants})))
[then] Set the Brands list for which coupons should not be applied {brands} = $bean.setBrandListToExclude((makeBrandList({merchants})))

