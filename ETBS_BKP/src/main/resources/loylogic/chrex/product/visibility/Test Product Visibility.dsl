[when]Do not Show Test Products {productCodes} to members other than Test members {testMemberIDs} = $bean:DisplayPropBean( programCode == "ETBS" && productCode in ({productCodes}) && memberID not in ({testMemberIDs}))

[then] Do not show the products =  $bean.setIsDisplayable(false);

[then] Apply your settings = update($bean);