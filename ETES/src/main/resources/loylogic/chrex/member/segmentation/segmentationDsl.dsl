[when] When the Member status test = $bean:ProgramMemberBean(programCode == "ETBS")

[when] When the Member status is Suspended = $bean:ProgramMemberBean(programCode == "ETBS", accountStatus in ("Pending","Cancelled"))

[when] When the Member status is not Suspended = $bean:ProgramMemberBean(programCode == "ETBS", accountStatus not in ("Pending","Cancelled"))


[then] Blacklist the Members = $bean.setIsBlackListed(true);

[then] Do not blacklist the Members = $bean.setIsBlackListed(false);

[then] Apply your settings = update($bean);