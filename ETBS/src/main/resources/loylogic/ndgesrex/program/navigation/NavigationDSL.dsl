[when] The segment code of member {segment} = $bean:DisplayPropBean(programCode == "ETBS", segmentCode in ({segment}))

[then] Set logo 2 URL{URL} = $bean.setLogoTwoUrl({URL});
[then] Set target {target} = $bean.setTarget({target});
[then] Application config URL{URL} = $bean.setApplicationUrl({URL});
[then] Apply your settings = update($bean);
