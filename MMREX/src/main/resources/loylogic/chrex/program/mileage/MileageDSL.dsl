[when] All products in {programCode} = $bean : MileageAlgoBean(programCode in ({programCode}))
[when] All products in {programCode} and redmption Types in {redemptionTypes} before {date} = $bean : MileageAlgoBean(programCode == "{programCode}",redemptionType in ({redemptionTypes}), (dateToday < "{date}"),  

[when] All products in {programCode} and redmption Types in {redemptionTypes} = $bean : MileageAlgoBean(programCode == "{programCode}",redemptionType in ({redemptionTypes}))  

[when] Some products in {programCode} need a special CPPM based on the product codes before {date} = $bean : MileageAlgoBean(programCode == "{programCode}", (dateToday < "{date}"),

[when] Default CPPM value for {programCode} and redmption Types not in {redemptionTypes} = $bean : MileageAlgoBean(programCode == "{programCode}", redemptionType not in ({redemptionTypes}),

[when] Product Specific CPPM value for {programCode} and redmption Types not in {redemptionTypes} = $bean : MileageAlgoBean(programCode == "{programCode}", redemptionType not in ({redemptionTypes}), 

[when] Including Products with codes {productCodes} = productCode in ({productCodes}))
[when] Excluding Products with codes {productCodes} = productCode not in ({productCodes}))
[when] But excluding Products with codes {productCodes} = productCode not in ({productCodes}),

[when] Including Products from Merchants with codes {merchantCodes} = merchantCode in ({merchantCodes}))
[when] Excluding Products from Merchants with codes {merchantCodes} = merchantCode not in ({merchantCodes}))
[when] Also excluding Products from Merchants with codes {merchantCodes} = merchantCode not in ({merchantCodes}))


[then] Set the Algorithm code to {algorithmCode} = $bean.setAlgorithmCode("{algorithmCode}");
[then] Set the CPPM to {conversionRatio} = $bean.setMilesConversionRatio({conversionRatio});              
[then] Set the Rounding type to {roundingType} = $bean.setMilesRoundingType("{roundingType}");
[then] Set the Rounding unit to {roundingUnit} = $bean.setMilesRoundingUnit({roundingUnit});
[then] Set the Floor price to {floorPrice} = $bean.setFloorPrice({floorPrice});
[then] Set the Ceiling price to {ceilingPrice} = $bean.setCeilingPrice({ceilingPrice});
[then] Set the Audit comments to {commentsForAudit} = $bean.setCommentsForAudit("{commentsForAudit}");
[then] Set the Brokerage Commission % to {brokrageCommissionProducts}= $bean.setBrokrageCommissionProducts({brokrageCommissionProducts});
[then] set the Loymart Sales Price Contribution Share % to {loymartSalesPriceContributionShare}= $bean.setLoymartSalesPriceContributionShare({loymartSalesPriceContributionShare});
[then] Set the Pricing Tier Code to {pricingTierCodes} = $bean.setPricingTierCode("{pricingTierCodes}");
[then] Set the Correction Factor value to {correctionValue} = $bean.setCorrectionFactor({correctionValue});

[then] Apply your settings = update($bean);