[when] All products in {programCode} = $bean : MileageAlgoBean(programCode == "{programCode}")

[when] All products in {programCode} for 2016 = $bean : MileageAlgoBean((programCode == "{programCode}"),(dateToday >= "01-Jan-2016"))

[when] Some products in {programCode} need a special CPPL based on the product codes = $bean : MileageAlgoBean(programCode == "{programCode}", 
[when] Including Products with codes {productCodes} = productCode in ({productCodes}))
[when] Excluding Products with codes {productCodes} = productCode not in ({productCodes}))


[then] Set the CPPL Offset % to {cpplOffsetPercent}= $bean.setCpplOffsetPercent({cpplOffsetPercent});
[then] Set the CPPL Offset to {cpplOffset}= $bean.setCpplOffset({cpplOffset});

[then] Set the Audit comments to {commentsForAudit} = $bean.setCommentsForAudit("{commentsForAudit}");

[then] Apply your settings = update($bean);