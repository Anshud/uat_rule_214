[when] Blacklist {tier} tier from transactions= $bean:ProgramMemberBean( (programCode == "JPREX"), tierName in ("{tier}"))
[when] Unverified email and mobile members blacklist= $bean:ProgramMemberBean( (programCode == "JPREX"), (isCustomer==true), eval(checkIfUnverified($bean)) )

[when] For {tier} tier blacklist for transaction= $bean:ProgramMemberBean( (programCode == "JPREX"), (isCustomer==true), tierName in ("{tier}"))


[when] Blacklist for JP transactions= $bean:ProgramMemberBean( (programCode == "JPREX"), (isCustomer==true), eval(checkIfEligible($bean)))

[when] When current Date is between 30 Jan 2018 and 05 Mar 2018 for JPREX = $bean:ProgramMemberBean( (programCode == "JPREX"), (dateToday >= "30-Jan-2018"), (dateToday <= "30-Dec-2018"))


[then] Set the promotional Content tag Max to {contentTag} = $bean.setContentTagMaximized("{contentTag}");
[then] Set the promotional Content tag Min to {contentTagMin} = $bean.setContentTagMinimized("{contentTagMin}");

[then] Blacklist the Members = $bean.setIsBlackListed(true);
[then] Do not blacklist the Members = $bean.setIsBlackListed(false);
[then] Set the Content tag to {contentTag} = $bean.setContentTag(setContentTagData($bean.isBlackListed,"{contentTag}"));

[then] Apply your settings = update($bean);