[when] When Visitor mode  and Categories in {catCodes} and basket country in {countryCodes} = $bean:DisplayPropBean( (programCode == "CHREX"), isCustomer == false , categoryCode in ({catCodes}) , countryInCart in ({countryCodes}))

[when] When Member access Categories in {catCodes} and basket country in {countryCodes} = $bean:DisplayPropBean(isDisplayable == true, programCode == "JPREX", categoryCode in  ({catCodes}) , countryInCart in ({countryCodes}))

[then] Make categories visible =  $bean.setIsDisplayable(true);
[then] Do not show the categories =  $bean.setIsDisplayable(false);
[then] Apply your settings = update($bean);