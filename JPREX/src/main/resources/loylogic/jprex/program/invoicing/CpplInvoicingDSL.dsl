[when] All products for {programCode} = $bean : MileageAlgoBean(programCode == "{programCode}")

[when] All products in {programCode} for countries in {countries} with RedemptionType {redemptionType} and has  DS-F and PO-F and PC-F = $bean : MileageAlgoBean((programCode == "{programCode}"), countryInCart in ({countries}), redemptionType in ({redemptionType}), isDirectSettlement == false , isProgramOwnedMerchant == false, isProgramCommisionAccountDetails == false)

[when] All products in {programCode} for countries in {countries} with RedemptionType {redemptionType} and has DS-T and PO-F and PC-F = $bean : MileageAlgoBean((programCode == "{programCode}"), countryInCart in ({countries}), redemptionType in ({redemptionType}), isDirectSettlement == true , isProgramOwnedMerchant == false, isProgramCommisionAccountDetails == false)

[when] All products in {programCode} for countries in {countries} with RedemptionType {redemptionType} and has DS-F and PO-T and PC-F = $bean : MileageAlgoBean((programCode == "{programCode}"), countryInCart in ({countries}), redemptionType in ({redemptionType}), isDirectSettlement == false , isProgramOwnedMerchant == true, isProgramCommisionAccountDetails == false)

[when] All products in {programCode} for countries in {countries} with RedemptionType {redemptionType} and has DS-F and PO-F and PC-T = $bean : MileageAlgoBean((programCode == "{programCode}"), countryInCart in ({countries}), redemptionType in ({redemptionType}),  isDirectSettlement == false , isProgramOwnedMerchant == false, isProgramCommisionAccountDetails == true)

[when] All products in {programCode} for countries in {countries} with RedemptionType {redemptionType} = $bean : MileageAlgoBean((programCode == "{programCode}"), countryInCart in ({countries}), redemptionType in ({redemptionType}))

[when] Including Products with codes {productCodes} = productCode in ({productCodes}))


[when] Excluding Products with codes {productCodes} = productCode in ({productCodes}))


[then] Set the CPPL Offset % to {cpplOffsetPercent}= $bean.setCpplOffsetPercent({cpplOffsetPercent});
[then] Set the CPPL Offset to {cpplOffset}= $bean.setCpplOffset({cpplOffset});

[then] Set the CPPL Offset to {cpplOffset}= $bean.setCpplOffset({cpplOffset});
[then] Set the Credit account Type {accountType}= $bean.setCreditAccountType({accountType});
[then] Set the Debit account Type {accountType}= $bean.setDebitAccountType({accountType});
[then] Set the debit account number to {debitAccountNumber}= $bean.setDebitAccountNumber({debitAccountNumber});
[then] Set the credit account number to {creditAccountNumber}= $bean.setCreditAccountNumber({creditAccountNumber});
[then] Set the payment Owner to {paymentOwner}= $bean.setPaymentOwner({paymentOwner});
[then] Set the settlement Currency to {settlementCCY} = $bean.setProgramSettlementCurrency({settlementCCY});

[then] Apply your settings = update($bean);