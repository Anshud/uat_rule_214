[when] All products in {programCode} and redmption Types in {redemptionTypes} = $bean : MileageAlgoBean(programCode == "{programCode}",redemptionType in ({redemptionTypes}))  

[when] Some products in {programCode} need a special CPPM based on the product codes = $bean : MileageAlgoBean(programCode == "{programCode}",

[when] When current Date is between 01 Feb and 28 Feb for JetPrevilige = (dateToday >= "01-Jan-2018"), (dateToday < "31-Jan-2018") ,
[when] When current Date is between 17 July and 31 Aug for JetPrivilege = (dateToday >= "17-Jul-2018"), (dateToday < "01-Sep-2018") ,
[when] When current Date is between {startDate} and {endDate} for JetPrivilege = (dateToday >= "{startDate}"),(dateToday < "{endDate}"),

[when] When product is a merchandize product = redemptionType == "B",redemptionSubType == "MR",
[when] When product is a Gift certificate = redemptionType == "B",redemptionSubType == "GC",

[when] Apply this rule only for country {isoCntCode} = countryCode == "{isoCntCode}",


[when] Including Products with codes {productCodes} = productCode in ({productCodes}))
[when] Excluding Products with codes {productCodes} = productCode not in ({productCodes}))

[when] Including Products from Merchants with codes {merchantCodes} = merchantCode in ({merchantCodes}))
[when] Excluding Products from Merchants with codes {merchantCodes} = merchantCode not in ({merchantCodes}))


[then] Set the Algorithm code to {algorithmCode} = $bean.setAlgorithmCode("{algorithmCode}");
[then] Set the CPPM to {conversionRatio} = $bean.setMilesConversionRatio(new Double({conversionRatio}));              
[then] Set the oldCPPM to {oldConversionRatio} = $bean.setMilesConversionRatioStrikeThrough({oldConversionRatio});
[then] Set the Rounding type to {roundingType} = $bean.setMilesRoundingType("{roundingType}");
[then] Set the Rounding unit to {roundingUnit} = $bean.setMilesRoundingUnit({roundingUnit});
[then] Set the Floor price to {floorPrice} = $bean.setFloorPrice(new Double({floorPrice}));
[then] Set the Audit comments to {commentsForAudit} = $bean.setCommentsForAudit("{commentsForAudit}");
[then] Set the Brokerage Commission % to {brokrageCommissionProducts}= $bean.setBrokrageCommissionProducts(new Float({brokrageCommissionProducts}));
[then] set the Loymart Sales Price Contribution Share % to {loymartSalesPriceContributionShare}= $bean.setLoymartSalesPriceContributionShare(new Float({loymartSalesPriceContributionShare}));
[then] Set the Pricing Tier Code to {pricingTierCodes} = $bean.setPricingTierCode("{pricingTierCodes}");
[then] Set the Correction Factor value to {correctionValue} = $bean.setCorrectionFactor(new Double({correctionValue}));

[then] Apply your settings = update($bean);