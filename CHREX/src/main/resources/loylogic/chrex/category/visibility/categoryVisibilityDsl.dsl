[when] When Visitor mode  and Categories in {catCodes} and basket country in {countryCodes} = $bean:DisplayPropBean( (programCode == "CHREX"), isCustomer == false , categoryCode in ({catCodes}) , countryInCart in ({countryCodes}))
[when] When Member status is Suspended or Inactive and Categories in {catCodes} = $bean:DisplayPropBean( (programCode == "CHREX"), accountStatus in ("Suspended" , "Inactive"), categoryCode in ({catCodes}))
[when] When Member Logged in and has no active reward card and Categories in {catCodes} and basket country in {countryCodes} = $bean:DisplayPropBean( (programCode == "CHREX"), isCustomer == true , eval($bean.getUserHasActiveRewardCard()) == false , categoryCode in  ({catCodes}) , countryInCart in ({countryCodes}))

[then] Make categories visible =  $bean.setIsDisplayable(true);
[then] Do not show the categories =  $bean.setIsDisplayable(false);
[then] Apply your settings = update($bean);