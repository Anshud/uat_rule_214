[when] For CHREX product visibility for the products {productCodes} and excluding members {memberIDs}  = $bean:DisplayPropBean( productCode in ({productCodes}), memberID not in ({memberIDs}))
[when] Do not show products {productCodes} to members which are not in segment {segmentId} = $bean:DisplayPropBean(productCode in ({productCodes}), segmentCode not in ({segmentId}))

[then] Make all products visible =  $bean.setIsDisplayable(true);
[then] Do not show the products =  $bean.setIsDisplayable(false);

[then] Apply your settings = update($bean);