[when] The member wants to transfer Choice Points to {transferID} = $bean : MileageAlgoBean(partnerTransferId =="{transferID}")


[then] Set the conversion ratio to {pointsTransferconversionRatio}:{pointsTransferconversionRatio1} = $bean.setPointsTransferconversionRatio("{pointsTransferconversionRatio}:{pointsTransferconversionRatio1}");

[then] Apply your settings = update($bean);