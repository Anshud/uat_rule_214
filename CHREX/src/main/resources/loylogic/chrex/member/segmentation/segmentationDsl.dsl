[when] When the Member status is Suspended or Inactive = $bean:ProgramMemberBean( programCode in ("CHREX"), accountStatus in ("Suspended" , "Inactive"))
[when] When the Member status is not Suspended = $bean:ProgramMemberBean( (programCode == "CHREX"), (accountStatus != "Suspended"))
[when] When current Date is between 2 Aug and 30 Oct for Choice = $bean:ProgramMemberBean( (programCode == "CHREX"), (dateToday >= "02-Aug-2017"), (dateToday <= "30-Oct-2018"))
[when] When program is {programID} member is blacklisted {memberid} = $bean:ProgramMemberBean( (programCode in ({programID})),memberID in ({memberid}))
[when] When member id of member is in {memberIds} = $bean:ProgramMemberBean( programCode in ("CHREX"), memberID in ({memberIds}))
[when] When member id of member = $bean:ProgramMemberBean((programCode in ("CHREX")))




[then] Blacklist the Members = $bean.setIsBlackListed(true);
[then] Do not blacklist the Members = $bean.setIsBlackListed(false);
[then] Set the Content tag to {contentTag} = $bean.setContentTag(setContentTagData($bean.isBlackListed,"{contentTag}"));
[then] Set the promotional Content tag Max to {contentTagMax} = $bean.setContentTagMaximized("{contentTagMax}");
[then] Set the promotional Content tag Min to {contentTagMin} = $bean.setContentTagMinimized("{contentTagMin}");
[then] Set the segmentCode of member {segmentCode} = $bean.setSegmentCode({segmentCode});
[then] Apply your settings = update($bean);