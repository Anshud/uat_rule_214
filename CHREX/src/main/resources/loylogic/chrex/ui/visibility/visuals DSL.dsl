[when] Make Key Visuals {keyVisualIds} visibile for Order PRC and Request VRC = $bean:DisplayPropBean(keyVisualId in ({keyVisualIds}), eval(checkUserStatus($bean,"A,DR")))

[when] Hide Key Visuals {keyVisualIds} if userstatus is in {userStatus} = $bean:DisplayPropBean(keyVisualId in ({keyVisualIds}), eval(checkUserStatusToBeBlocked($bean,{userStatus})))
[when] Hide Key Visuals {keyVisualIds} if Member status is Suspended or Inactive  = $bean:DisplayPropBean( keyVisualId in ({keyVisualIds}),(programCode == "CHREX"), accountStatus in ("Suspended" , "Inactive"))

[when] Hide Key Visuals {keyVisualIds} if userstatus is in {userStatus} and cardstatus is {cardStatus} for PC = 
$bean:DisplayPropBean(keyVisualId in ({keyVisualIds}), eval(checkUserStatusToBeBlocked($bean,{userStatus})), eval(checkCardStatus($bean,{cardStatus},"P")))

[when] Hide Key Visuals {keyVisualIds} if userstatus is in {userStatus} and cardstatus is {cardStatus} for VC = 
$bean:DisplayPropBean(keyVisualId in ({keyVisualIds}), eval(checkUserStatusToBeBlocked($bean,{userStatus})), eval(checkCardStatus($bean,{cardStatus},"V")))

[when] Make Key Visuals {keyVisualIds} visibile for Start Shopping = $bean:DisplayPropBean(keyVisualId in ({keyVisualIds}), !eval(checkUserStatus($bean,"A")), eval(checkCardStatus($bean,"A","P")))


[when] Make Key Visuals {keyVisualIds} hidden for Blacklisted country = $bean:DisplayPropBean(keyVisualId in ({keyVisualIds}), eval(checkRenewalFlag($bean)))

[when] Make Key Visuals {keyVisualIds} visibile for Load VRC or Order PC= $bean:DisplayPropBean(keyVisualId in ({keyVisualIds}), eval(checkUserStatus($bean,"A")), eval(checkCardStatus($bean,"A","V")))

[then] Make all products visible =  $bean.setIsDisplayable(true);
[then] Do not show the products =  $bean.setIsDisplayable(false);

[then] Apply your settings = update($bean);