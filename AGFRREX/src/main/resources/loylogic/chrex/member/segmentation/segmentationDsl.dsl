[when] When the Member country {countryCode} = $bean:ProgramMemberBean( programCode == "AGFRREX", countryCode in ({countryCode}))


[then] Set the segmentCode of member {segmentCode} = $bean.setSegmentCode({segmentCode});
[then] Apply your settings = update($bean);