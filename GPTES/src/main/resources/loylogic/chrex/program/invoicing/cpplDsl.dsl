[when] All products in {programCode} = $bean : MileageAlgoBean(programCode == "{programCode}")

[when] Some products in {programCode} need a special CPPL based on the product codes = $bean : MileageAlgoBean(programCode == "{programCode}", 
[when] Including Products with codes {productCodes} = productCode in ({productCodes}))

[when] All Merchandise products in {programCode}  = $bean : MileageAlgoBean(programCode in ("{programCode}"), redemptionType in ("B"), redemptionSubType in ("MR"))
[when] All Giftcard and Exp products in {programCode}  = $bean : MileageAlgoBean(programCode in ("{programCode}"), redemptionType in ("B"), redemptionSubType in ("GC", "EX", "EV"))
[when] All Raffle products in {programCode}  = $bean : MileageAlgoBean(programCode in ("{programCode}"), redemptionType in ("R"))
[when] All dontion products in {programCode}  = $bean : MileageAlgoBean(programCode in ("{programCode}"), redemptionType in ("G"))


[then] Set the CPPL Offset % to {cpplOffsetPercent}= $bean.setCpplOffsetPercent({cpplOffsetPercent});
[then] Set the CPPL Offset to {cpplOffset}= $bean.setCpplOffset({cpplOffset});

[then] Set the Audit comments to {commentsForAudit} = $bean.setCommentsForAudit("{commentsForAudit}");

[then] Apply your settings = update($bean);