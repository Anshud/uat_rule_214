[when]Comma to be added after condition = ,
[when]Closing bracket after condition = )
[when]Program code condition validation = $bean:BuyGiftPointsBean(programCode == "GPTES"
[when]Participating program code condition validation {participatingProgramCode} = participatingProgramCode == {participatingProgramCode}
[when]Module condition validation {module} = module == {module}
[when]Points value condition validation {Startpoints} to {Endpoint} = eval(checkValidPointsFunction(($bean),{Startpoints},{Endpoint}))
[when]Promotion code empty validation = promoCode == null
[when]Start date {startDate} end date {endDate} valdiation = eval(checkPromoExpiry($bean, {startDate}, {endDate}))
[when]Promo code condition validation {promoCodes} = eval(checkPromoCodeValidity($bean, ({promoCodes})))


[then] Set generic CPP value {genricCPP}= $bean.setGenricCPP({genricCPP});
[then] Set buy CPP value {buyCPP} = $bean.setBuyCPP({buyCPP});
[then] Set limit per member value {limitPerMember} = $bean.setLimitPerMember({limitPerMember});
[then] Set order limit value {orderLimit} = $bean.setOrderLimit({orderLimit});
[then] Set promotion percentage client {clientPro} loylogic {loylogicPro} within {startDate} to {endDate}= createPromotionPercentageFunction($bean,{clientPro},{loylogicPro},{startDate},{endDate});
[then] Set promotion Client Percent {promotionClientPercent} = $bean.setPromotionClientPercent({promotionClientPercent});
[then] Set promotion LL Percent {promotionLLPercent} = $bean.setPromotionLLPercent({promotionLLPercent});
