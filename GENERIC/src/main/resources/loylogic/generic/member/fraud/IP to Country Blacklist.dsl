[when] When the member ip country belongs to Delivery address country to blacklist and programs in {program} = $bean:FraudManagementBean( $score:fraudScore,programCode in ({program}), eval(checkIpToCountryBlacklist(orderCountry, deliveryAddrCountry)))

[when] When the member ip country belongs to Delivery address country to blacklist and programs not in {program} = $bean:FraudManagementBean( $score:fraudScore, programCode not in ({program}), eval(checkIpToCountryBlacklist(orderCountry, deliveryAddrCountry)))

[when] When the member ip country belongs to Profile address country to blacklist and programs in {program} = $bean:FraudManagementBean( $score:fraudScore, programCode in ({program}), eval(checkIpToCountryBlacklist(orderCountry, countryCode)))

[when] When the member ip country belongs to Profile address country to blacklist and programs not in {program} = $bean:FraudManagementBean( $score:fraudScore, programCode not in ({program}), eval(checkIpToCountryBlacklist(orderCountry, countryCode)))

[when] IP country in grey list {countries} = $bean:FraudManagementBean($score:fraudScore, eval(checkIpToCountryWithGreyList(orderCountry, "{countries}")))

[when] Earnshops IP country to grey list {countries} = $bean:FraudManagementBean($score:fraudScore, eval(checkIpToCountryWithGreyList(orderCountry, "{countries}")),programCode in ("ETHACCSHP","HHES","ETES"))

[then] Increment Fraud Score by {fraudScore} = $bean.setFraudScore($score + new Integer({fraudScore}))
[then] Set ip to delivery country individualFraudScores {newScore} = $bean.setIndividualFraudScores(manageFraudScores("IP2COUNTRY_VS_DELIVERY_COUNTRY", $bean, ("IP Country: "+ $bean.getOrderCountry() + " || Delivery Country: " + $bean.getDeliveryAddrCountry()),{newScore}))
[then] Set ip to profile country individualFraudScores {newScore} = $bean.setIndividualFraudScores(manageFraudScores("IP2COUNTRY_VS_PROFILE_COUNTRY", $bean, ("IP Country: "+ $bean.getOrderCountry() + " || Profile Country: " + $bean.getCountryCode()),{newScore}))
[then] Set ip Country in grey list individualFraudScores {newScore} = $bean.setIndividualFraudScores(manageFraudScores("IP2COUNTRY_IN_GREY_LIST", $bean, ("IP Country: "+ $bean.getOrderCountry()),{newScore}))