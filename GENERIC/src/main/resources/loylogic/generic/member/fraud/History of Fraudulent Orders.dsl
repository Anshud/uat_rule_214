[when] When the Delivery address is blacklisted delivery address History and programs not in {programs} = $bean:FraudManagementBean($score:fraudScore, isFraudDeliveryAddress == true,programCode not in ({programs}))

[when] When the Delivery address is blacklisted delivery address History and programs in {programs} = $bean:FraudManagementBean($score:fraudScore, isFraudDeliveryAddress == true,programCode in ({programs}))

[when] When the Email address is blacklisted in fraud Email address History = $bean:FraudManagementBean($score:fraudScore, isFraudEmailAddress == true)

[when] When the Same Member with different Delivery address History = $bean:FraudManagementBean($score:fraudScore, isSameMemDiffDlvAddr == true)

[when] When the Fraud failed ordes count exceeds blacklisted limit = $bean:FraudManagementBean($score:fraudScore, fraudOrderCount >= 1)


[then] Increment Fraud Score by {fraudScore} = $bean.setFraudScore($score + new Integer({fraudScore}))
[then] Set is Fraud Delivery Address individualFraudScores {newScore} = $bean.setIndividualFraudScores(manageFraudScores("HISTORY_OF_FRAUDULENT_ORDERS_DELIVERY_ADDRESS", $bean, $bean.getIsFraudDeliveryAddress().toString(),{newScore}))
[then] Set is Fraud Eamil Address individualFraudScores {newScore} = $bean.setIndividualFraudScores(manageFraudScores("HISTORY_OF_FRAUDULENT_ORDERS_EMAIL_ADDRESS", $bean, $bean.getIsFraudEmailAddress().toString(),{newScore}))
[then] Set is Same Member Different Delivery Address individualFraudScores {newScore} = $bean.setIndividualFraudScores(manageFraudScores("SAME_MEMBER_DIFFERENT_ADDRESS", $bean, $bean.getIsSameMemDiffDlvAddr().toString(),{newScore}))
[then] Set Fraud Order Count individualFraudScores {newScore} = $bean.setIndividualFraudScores(manageFraudScores("HISTORY_OF_FRAUDULENT_ORDERS_ORDER_COUNT", $bean, $bean.getFraudOrderCount().toString(),{newScore}))