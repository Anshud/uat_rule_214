[when] When the member email address belongs to the blacklisted email group including programs {programs} = $bean:FraudManagementBean($score:fraudScore,programCode in ({programs}))

[when] When the member email address belongs to the blacklisted email group excluding programs {programs} = $bean:FraudManagementBean($score:fraudScore,programCode not in ({programs}), 


[when] from blacklisted email group = eval(checkEmailBlacklist(deliveryAddrPrimaryEmailId, deliveryAddrSecEmailId, "kaustubhgoswami1234@yopmail.com")))

[when] When the member email address domain belongs to the grey list domain group and programs not in {programs} = $bean:FraudManagementBean($score:fraudScore,programCode not in ({programs}),eval(checkEmailDomainBlacklist(deliveryAddrPrimaryEmailId, deliveryAddrSecEmailId, "@yopmail.com,@yahoo.com,@yahoo.co.uk,@outlook.com,@rediffmail.com,@hotmail.com,@gmail.com,tiscali.it,tiscalinet.it")))

[when] When the member email address domain belongs to the grey list domain group and programs in {programs} = $bean:FraudManagementBean($score:fraudScore,programCode in ({programs}),eval(checkEmailDomainBlacklist(deliveryAddrPrimaryEmailId, deliveryAddrSecEmailId, "@yahoo.com,@yahoo.co.in,@yahoo.co.uk,@outlook.com,@rediffmail.com,@hotmail.com,@gmail.com,tiscali.it,tiscalinet.it")))

[when] When the member email address domain belongs to the grey list domain group and program is MMBS = $bean:FraudManagementBean($score:fraudScore,programCode in ("MMBS"),eval(checkEmailDomainBlacklist(deliveryAddrPrimaryEmailId, deliveryAddrSecEmailId, "tiscali.it,tiscalinet.it")))

[when] When the member email address contains blacklisted String and programs in {programs} = $bean:FraudManagementBean($score:fraudScore,programCode in ({programs}), eval(checkEmailForBlacklistString(deliveryAddrPrimaryEmailId, deliveryAddrSecEmailId, "xnowblack")))

[when] When the ProgramCode is in {programCode} = $bean:FraudManagementBean(programCode in ({programCode}),

[when] When the member profile email address is null = email in (null,"null",""))

[then] Increment Email Fraud Score by {fraudScore} = $bean.setFraudScore($bean.getFraudScore() + new Integer({fraudScore}))
[then] Set Email individualFraudScores {newScore} = $bean.setIndividualFraudScores(manageFraudScores("BLACKLIST_EMAIL_ADDRESS", $bean, ignoreNullValues($bean.getDeliveryAddrPrimaryEmailId()+", "+$bean.getDeliveryAddrSecEmailId()),{newScore}))

[then] Set Greylist Email individualFraudScores {newScore} = $bean.setIndividualFraudScores(manageFraudScores("GREYLIST_EMAIL_ADDRESS", $bean, ignoreNullValues($bean.getDeliveryAddrPrimaryEmailId()+", "+$bean.getDeliveryAddrSecEmailId()),{newScore}))

[then] Set Email Domain individualFraudScores {newScore} = $bean.setIndividualFraudScores(manageFraudScores("BLACKLIST_EMAIL_DOMAIN_ADDRESS", $bean, ignoreNullValues($bean.getDeliveryAddrPrimaryEmailId()+", "+$bean.getDeliveryAddrSecEmailId()),{newScore}))
[then] Set No Profile Email individual FraudScores {newScore} = $bean.setIndividualFraudScores(manageFraudScores("NO_PROFILE_EMAIL_ADDRESS", $bean, $bean.getEmail(),{newScore}))


[then] Set Email blacklist String individualFraudScores {newScore} = $bean.setIndividualFraudScores(manageFraudScores("BLACKLIST_EMAIL_TEXT", $bean, ignoreNullValues($bean.getDeliveryAddrPrimaryEmailId()+", "+$bean.getDeliveryAddrSecEmailId()),{newScore}))