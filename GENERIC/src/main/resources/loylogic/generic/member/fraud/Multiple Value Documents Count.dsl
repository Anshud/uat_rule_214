[when] When the value document count in the same order exceeds blacklisted limit excluding ETH {VDcount}= $bean:FraudManagementBean($score:fraudScore, (orderValueDocumentCount > {VDcount}),programCode not in ("ETH"))

[when] When the value document count in the same order exceeds blacklisted limit for ETH {VDcount}= $bean:FraudManagementBean($score:fraudScore, (orderValueDocumentCount >= {VDcount}),programCode in ("ETH"))

[then] Increment Fraud Score by {fraudScore} = $bean.setFraudScore($score + new Integer({fraudScore}))
[then] Set Order Value Document Count individualFraudScores {newScore} and {threshold} = $bean.setIndividualFraudScores(manageFraudScores("MULTIPLE_VALUE_DOCUMENTS_IN_SAME_ORDER", $bean, "No. of units: "+$bean.getOrderValueDocumentCount().toString()+" || Threshold: "+{threshold},{newScore}))