[when] Member card scheme belongs to the suspected cards group {cardTypes} and excluding programs {programCode} = $bean:FraudManagementBean( $score:fraudScore, programCode not in ({programCode}), paymentCreditCardType in ({cardTypes}))

[then] Increment Fraud Score by {fraudScore} = $bean.setFraudScore($score + new Integer({fraudScore}))
[then] Set Member CardScheme individualFraudScores {newScore} = $bean.setIndividualFraudScores(manageFraudScores("MEMBER_CARD_SCHEME", $bean, $bean.getPaymentCreditCardType(),{newScore}))