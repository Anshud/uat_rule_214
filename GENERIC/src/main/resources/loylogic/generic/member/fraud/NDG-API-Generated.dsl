[when] When for NDG the order has products for {programCode} do not put order on Hold productcode inclucing {productCodes} = $bean:FraudManagementBean($score:fraudScore, programCode in ({programCode}), eval(checkOrderForAllVoucherProductCodes($bean,{productCodes})))

[then] Increment for NDG Fraud Score by {fraudScore} = $bean.setFraudScore($bean.getFraudScore() + new Integer({fraudScore}))

[then] Set for NDG individualFraudScores for Orders {newScore} = $bean.setIndividualFraudScores(manageFraudScores("DO_NOT_HOLD_API_GEN_ORDER_RULE", $bean,"",{newScore}))