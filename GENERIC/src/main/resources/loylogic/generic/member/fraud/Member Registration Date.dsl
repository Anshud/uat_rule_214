[when] When the member enrollment date lessthan 30 days and excluded {program} programs  = $bean:FraudManagementBean( $score:fraudScore, programCode not in ({program}), eval(checkEnrollmentDate(enrollmentDate)))

[then] Increment Fraud Score by {fraudScore} = $bean.setFraudScore($score + new Integer({fraudScore}))
[then] Set Enrollment Date individualFraudScores {newScore} = $bean.setIndividualFraudScores(manageFraudScores("MEMBER_ENROLLMENT_DATE", $bean, dateToString($bean.getEnrollmentDate()),{newScore}))