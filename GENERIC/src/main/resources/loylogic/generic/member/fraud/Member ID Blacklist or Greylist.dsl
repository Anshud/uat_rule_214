[when] When the SAS member ID belongs to the blacklisted id group = $bean:FraudManagementBean($score:fraudScore,(programCode in ("SAS") && memberID in ("EB620615344")))

[when] When the ETH member ID belongs to the blacklisted id group = $bean:FraudManagementBean($score:fraudScore,(programCode in ("ETH","ETHACCSHP") && memberID in ("105736114875","105736106173","105735793144","105736131900","105736132515","105735787603","105733059541","105735394166","105735683336","105735787706","105736030131","105735465662","105735401240","105735507625","100269269306","100268437463","100273530803","100275819442","100273698571","105735835030","105736038155","105736386932","105736384891","105736504823","105740532155","105740531805","105736386210","105756180283","105756180471","105756181333","105756181484","105756184973","105756184096","105756184542","105756180806","105756185021","105756028140","105735681984")))

[when] When the HHES member ID belongs to the blacklisted id group = $bean:FraudManagementBean($score:fraudScore,(programCode in ("HHES") && memberID in ("632955465")))


[when] When the MMBS member ID belongs to the Greylisted id group = $bean:FraudManagementBean($score:fraudScore,(programCode in ("MMBS") && memberID in ("0000060531")))

[when] When the ETBS member ID belongs to the Greylisted id group = $bean:FraudManagementBean($score:fraudScore,(programCode in ("ETBS") && memberID in ("00146147938")))

[when] When the CHREX member ID belongs to the blacklisted id group = $bean:FraudManagementBean($score:fraudScore && programCode == "CHREX" && memberID in ("loyautomation2@gmail.com"))
[when] When the SCCRREX member ID belongs to the blacklisted id group = $bean:FraudManagementBean($score:fraudScore && programCode == "SCCRREX" && memberID in ("0132000011000"))

[then] Increment Fraud Score by {fraudScore} = $bean.setFraudScore($score + new Integer({fraudScore}))
[then] Set Member ID individualFraudScores {newScore} = $bean.setIndividualFraudScores(manageFraudScores("BLACKLIST_MEMBER_IDS", $bean, $bean.getMemberID(),{newScore}))
[then] Set Greylisted Member ID individualFraudScores {newScore} = $bean.setIndividualFraudScores(manageFraudScores("GREYLIST_MEMBER_IDS", $bean, $bean.getMemberID(),{newScore}))