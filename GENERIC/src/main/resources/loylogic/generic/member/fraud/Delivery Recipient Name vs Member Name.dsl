[when] When the member recipient first name equals to profile first name and program code not in {programCodes} = $bean:FraudManagementBean($score:fraudScore, programCode not in ({programCodes}),eval(compareFirstNames($bean, deliveryAddrFirstName, deliveryAddrLastName)))
[when] When the member profile first name does not contains recipient first name and programCode in {programCode} = $bean:FraudManagementBean($score:fraudScore, eval(firstNameSubStringOfMemFirstName($bean, deliveryAddrFirstName, deliveryAddrLastName)),programCode in ({programCode}))

[when] When the member recipient last name equals to profile last name and program code not in  {programCodes} = $bean:FraudManagementBean($score:fraudScore, programCode not in ({programCodes}),eval(compareLastNames($bean, deliveryAddrFirstName, deliveryAddrLastName)))
[when] When the member profile last name does not contains recipient last name and programCode in {programCode} = $bean:FraudManagementBean($score:fraudScore, eval(lastNameSubStringOfMemLastName($bean, deliveryAddrFirstName, deliveryAddrLastName)),programCode in ({programCode}))

[then] Increment Fraud Score by {fraudScore} = $bean.setFraudScore($score + new Integer({fraudScore}))
[then] Set Member first name individual FraudScores {newScore} = $bean.setIndividualFraudScores(manageFraudScores("MEMBER_FIRST_NAME_DIFFERENT_FROM_RECIPIENT", $bean, ("Recipient first name: "+$bean.getDeliveryAddrFirstName()+" || "+"Profile first name: "+$bean.getFirstName()),{newScore}))

[then] Set Member last name individual FraudScores {newScore} = $bean.setIndividualFraudScores(manageFraudScores("MEMBER_LAST_NAME_DIFFERENT_FROM_RECIPIENT", $bean, ("Recipient last name: "+$bean.getDeliveryAddrLastName()+" || "+"Profile last name: "+$bean.getLastName()),{newScore}))