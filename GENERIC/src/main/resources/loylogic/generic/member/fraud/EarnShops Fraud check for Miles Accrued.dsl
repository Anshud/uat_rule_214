[when] When Ratio of Miles Accrued to Cash Paid for Earnshop is more than the {threshold} for {programCodes}= $bean:FraudManagementBean($score:fraudScore,eval(checkRatioOfMilesAccruedCashPaid($bean,{threshold})),programCode in ({programCodes}))


[then] Increment Fraud Score by {fraudScore} = $bean.setFraudScore($bean.getFraudScore() + new Integer({fraudScore}))
[then] Set individualFraudScores for Earnshop Fraud {newScore} with threshold as {threshold} = $bean.setIndividualFraudScores(manageFraudScores("MILES_ACCRUAL_FRAUD_FOR_EARNSHOP", $bean,"Ratio: "+($bean.getTotalEarnMilesInBasket()/$bean.getOrderAbsoluteCash())+" || Threshold: "+{threshold},{newScore}))