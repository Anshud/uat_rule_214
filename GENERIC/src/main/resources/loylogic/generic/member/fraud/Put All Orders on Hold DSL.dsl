[when] When order is placed on Voucher Shop = $bean:FraudManagementBean($score:fraudScore,programCode == "GGCRC")

[when] When order is placed on Akruu = $bean:FraudManagementBean($score:fraudScore,programCode == "GPTES")

[when] When order is placed on JetPrivilege = $bean:FraudManagementBean($score:fraudScore,programCode == "JPREX")

[when] When order is placed on EYREX program = $bean:FraudManagementBean($score:fraudScore,programCode in ("EYREX"))

[when] When order is placed on {programCode} program = $bean:FraudManagementBean($score:fraudScore,programCode == {programCode})

[when] When order is placed on topbonus = $bean:FraudManagementBean($score:fraudScore,programCode == "ABBS")

[when] When order is placed on dacadoo = $bean:FraudManagementBean($score:fraudScore,programCode == "DDREX")

[when] When order is placed on JMREX = $bean:FraudManagementBean($score:fraudScore,programCode == "JMREX")

[when] When order is placed on ETH = $bean:FraudManagementBean($score:fraudScore,programCode == "ETH")

[then] Increment Fraud Score {fraudScore} = $bean.setFraudScore($bean.getFraudScore + new Integer({fraudScore}))

[then] Set Fraud Scores for Voucher Shop orders {newScore} = $bean.setIndividualFraudScores(manageFraudScores("PUT_ALL_ORDER_ON_HOLD_FOR_VOUCHER_SHOP", $bean, "",{newScore}))

[then] Set Fraud Scores for Akruu orders {newScore} = $bean.setIndividualFraudScores(manageFraudScores("PUT_ALL_ORDER_ON_HOLD_FOR_AKRUU", $bean, "",{newScore}))

[then] Set Fraud Scores for topbonus orders {newScore} = $bean.setIndividualFraudScores(manageFraudScores("PUT_ALL_ORDER_ON_HOLD_FOR_TOPBONUS_SHOP", $bean, "",{newScore}))

[then] Set Fraud Scores for JetPrivilege orders {newScore} = $bean.setIndividualFraudScores(manageFraudScores("PUT_ALL_ORDER_ON_HOLD_FOR_JP", $bean, "",{newScore}))

[then] Set Fraud Scores for EYREX orders {newScore} = $bean.setIndividualFraudScores(manageFraudScores("PUT_ALL_ORDER_ON_HOLD_FOR_EYREX", $bean, "",{newScore}))

[then] Set Fraud Scores to {newScore} for QNB = $bean.setIndividualFraudScores(manageFraudScores("PUT_ALL_ORDER_ON_HOLD_FOR_QNB", $bean, "",{newScore}))

[then] Set Fraud Scores for DDREX orders {newScore} = $bean.setIndividualFraudScores(manageFraudScores("PUT_ALL_ORDER_ON_HOLD_FOR_DDREX", $bean, "Put All Orders on Hold for dacadoo",{newScore}))

[then] Set Fraud Scores for ETH orders {newScore} = $bean.setIndividualFraudScores(manageFraudScores("PUT_ALL_ORDER_ON_HOLD_FOR_ETH", $bean, "",{newScore}))

[then] Set Fraud Scores for JMREX orders {newScore} = $bean.setIndividualFraudScores(manageFraudScores("PUT_ALL_ORDER_ON_HOLD_FOR_JMREX", $bean, "Put All Orders on Hold for JMREX",{newScore}))