[when] Perform Member ID based Velocity check against last {threshold} orders including {programs} = $bean:FraudManagementBean($score:fraudScore, programCode in ({programs}), eval(checkVelocityParams("VELOCITY_MEMBER_ID", $bean, {threshold})))

[when] Perform Member ID based Velocity check against last {threshold} orders excluding {programs} = $bean:FraudManagementBean($score:fraudScore, programCode not in ({programs}), eval(checkVelocityParams("VELOCITY_MEMBER_ID", $bean, {threshold})))

[then] Increment Fraud Score by {newScore} = $bean.setFraudScore($score + new Integer({newScore}))
[then] Set Audit comments for MemberID with score {fraudScore} against threshold of {threshold} = $bean.setIndividualFraudScores(manageFraudScores("VELOCITY_MEMBER_ID", $bean, ("Velocity: "+ $bean.getVelocityMemberId()+" || "+ "Threshold: "+ {threshold}), {fraudScore}))




[when] Perform IP based Velocity check against last {threshold} orders including {programs}  = $bean:FraudManagementBean($score:fraudScore, programCode in ({programs}),eval(checkVelocityParams("VELOCITY_IP", $bean, {threshold})))

[when] Perform IP based Velocity check against last {threshold} orders excluding {programs}  = $bean:FraudManagementBean($score:fraudScore, programCode not in ({programs}),eval(checkVelocityParams("VELOCITY_IP", $bean, {threshold})))

[then] Increment Fraud Score by {newScore} = $bean.setFraudScore($score + new Integer({newScore}))
[then] Set Audit comments for IP with score {fraudScore} against threshold of {threshold} = $bean.setIndividualFraudScores(manageFraudScores("VELOCITY_IP", $bean, ("Velocity: "+ $bean.getVelocityIpAddress()+" || "+ "Threshold: "+ {threshold}), {fraudScore}))




[when] Perform Email Address based Velocity check against last {threshold} orders including {programs}  = $bean:FraudManagementBean($score:fraudScore,programCode in ({programs}), eval(checkVelocityParams("VELOCITY_EMAIL_ADDRESS", $bean, {threshold})))

[when] Perform Email Address based Velocity check against last {threshold} orders excluding {programs}  = $bean:FraudManagementBean($score:fraudScore,programCode not in ({programs}), eval(checkVelocityParams("VELOCITY_EMAIL_ADDRESS", $bean, {threshold})))

[then] Increment Fraud Score by {newScore} = $bean.setFraudScore($score + new Integer({newScore}))
[then] Set Audit comments for Email address with score {fraudScore} against threshold of {threshold} = $bean.setIndividualFraudScores(manageFraudScores("VELOCITY_EMAIL_ADDRESS", $bean, ("Velocity: "+ $bean.getVelocityEmailAddress()+" || "+ "Threshold: "+ {threshold}), {fraudScore}))




[when] Perform Delivery Address based Velocity check against last {threshold} orders  orders including {programs}  = $bean:FraudManagementBean($score:fraudScore, programCode in ({programs}), eval(checkVelocityParams("VELOCITY_DELIVERY_ADDRESS", $bean, {threshold})))

[when] Perform Delivery Address based Velocity check against last {threshold} orders  orders excluding {programs}  = $bean:FraudManagementBean($score:fraudScore, programCode not in ({programs}), eval(checkVelocityParams("VELOCITY_DELIVERY_ADDRESS", $bean, {threshold})))

[then] Increment Fraud Score by {newScore} = $bean.setFraudScore($score + new Integer({newScore}))
[then] Set Audit comments for Delivery address with score {fraudScore} against threshold of {threshold} = $bean.setIndividualFraudScores(manageFraudScores("VELOCITY_DELIVERY_ADDRESS", $bean, ("Velocity: "+ $bean.getVelocityDeliveryAddress()+" || "+ "Threshold: "+ {threshold}), {fraudScore}))
