[when]For Configuring the Threshold Value = $bean:FraudManagementBean()


[then]Set the threshold {threshold} value For Edit Distance calculation for Blacklisted Delivery Address = $bean.setDeliveryAddrThresholdForEditDistance(new Integer({threshold}))
[then]Set the Fraud Score to be assigened for Blacklisted Delivery Address by {fraudScore} = $bean.setFraudScoreToBeAssigenedForDelvAddr(new Integer({fraudScore}))