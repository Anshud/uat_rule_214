[when] When the Delivery Address of Different Members are Same excluding {programs} = $bean:FraudManagementBean($score:fraudScore, programCode not in ({programs}),sameDeliveryAddrDifferentMember == true)

[then] Increment Fraud Score by {fraudScore} = $bean.setFraudScore($score + new Integer({fraudScore}))
[then] Set Audit Comments if Delivery Address of Different Members are Same {newScore} = $bean.setIndividualFraudScores(manageFraudScores("SAME_ADDRESS_DIFFERENT_MEMBER", $bean, "", {newScore}))