[when] When the Fraud Score goes above the Abort threshold {fraudScore} and excluding for specific product Codes {prdCodes} = $bean:FraudManagementBean(fraudScore >= {fraudScore}, eval(checkExcessBaggageProducts($bean,{prdCodes})) )

[when] When the Fraud Score goes above the Hold threshold {fraudScore1} except merchnat {merCodes} orders = $bean:FraudManagementBean(fraudScore >= {fraudScore1}, (programCode == "SAS" && eval(!checkMerchantCodes($bean,{merCodes}))))


[when] For JP, when the Fraud Score goes above the Hold threshold {fraudScore1} = $bean:FraudManagementBean(fraudScore >= {fraudScore1} && programCode == "JPREX")

[when] When the Fraud Score goes above the Hold threshold {fraudScore1} and excluding for specific product Codes {prdCodes} = $bean:FraudManagementBean(fraudScore >= {fraudScore1}, (programCode == "ETH" &&  eval(checkProductCodes($bean,{prdCodes}))))

[when] When the Fraud Score goes above the Hold threshold {fraudScore1} = $bean:FraudManagementBean(fraudScore >= {fraudScore1} && programCode not in ("SAS","EYREX","ABBS"))

[when] For ProgramCode as {programCode} = $bean:FraudManagementBean(programCode == {programCode})

[when] Abort MMBS Orders from 27 Sep = $bean:FraudManagementBean($score:fraudScore, (programCode == "MMBS") ,(dateToday >= "27-Sep-2017"))

[when] Hold order if the cityName is {cityName} for {program} and threshold {threshold} = $bean:FraudManagementBean($score:fraudScore, programCode in ({program}), eval(checkCityName($bean,{cityName}, {threshold})) )


[when] When the ProgramCode is {programCode} = $bean:FraudManagementBean(programCode == {programCode},

[when] Including members with memberIDs {memberIDs} = memberID in ({memberIDs}))


[then] Abort the order = $bean.setNextDeliveryStatus("A")

[then] Hold the order = $bean.setNextDeliveryStatus("H")

[then] Increment Fraud Score for ABBS {fraudScore} = $bean.setFraudScore($bean.getFraudScore + new Integer({fraudScore}))

[then] Set Fraud Scores for ABBS Fraud Members {newScore} = $bean.setIndividualFraudScores(manageFraudScores("PUT_ORDER_ON_HOLD_FOR_ABBS_FRAUD_MEMBERS", $bean, "",{newScore}))

[then] Set Fraud Scores for MM Closure Activity {newScore} = $bean.setIndividualFraudScores(manageFraudScores("MM_CLOSURE", $bean, "",new Integer({newScore})))

[then] Set Fraud Scores for CityName Based Fraud {newScore} = $bean.setIndividualFraudScores(manageFraudScores("PUT_ORDER_ON_HOLD_FOR_CITYNAME_BASED_FRAUD", $bean, (onlyCharsAndDigits($bean.getDeliveryAddrCity()).toString()),{newScore}))