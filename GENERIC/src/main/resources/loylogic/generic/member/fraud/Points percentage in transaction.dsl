[when] When the Points percentage involved in the transaction below blacklisted percentage {MilesPerc} excluding programs {programs}= $bean:FraudManagementBean($score:fraudScore, (orderMilesPercentage < {MilesPerc}),programCode not in ({programs}) )

[when] Skip Miles Only orders {MilesPerc}= $bean:FraudManagementBean($score:fraudScore, (orderMilesPercentage == {MilesPerc}))

[then] Increment Fraud Score by {fraudScore} = $bean.setFraudScore($score + new Integer({fraudScore}))
[then] Set Points percentage individualFraudScores {newScore} and {threshold} = $bean.setIndividualFraudScores(manageFraudScores("POINTS_INVOLVED_IN_TRANSACTION_PERCENTAGE", $bean, "Points component: "+$bean.getOrderMilesPercentage().toString()+" || Threshold: "+{threshold},{newScore}))

[then] Set Points only orders individualFraudScores {newScore} and {threshold} = $bean.setIndividualFraudScores(manageFraudScores("POINTS_ONLY_TRANSACTION", $bean, "Points component: "+$bean.getOrderMilesPercentage().toString()+" || Threshold: "+{threshold},{newScore}))