[when] When the order belongs the merchnat {merCodes} = $bean:FraudManagementBean( $score:fraudScore, (programCode in ("ETH") && eval(checkMerchantCodes($bean,{merCodes}))))

[when] When the Product quantity is {qty} or more = $bean:FraudManagementBean( $score:fraudScore, eval(checkForProductQuantity($bean,{qty})))


[then] Increment Fraud Score by {fraudScore} = $bean.setFraudScore($score + new Integer({fraudScore}))
[then] Set Merchant specific individualFraudScores {newScore} and {merchantCodes} = $bean.setIndividualFraudScores(manageFraudScores("MERCHANT_ORDERS_ON_HOLD", $bean, {merchantCodes},{newScore}))
[then] Set product quantity specific individualFraudScores {newScore} and {qty} = $bean.setIndividualFraudScores(manageFraudScores("PRODUCT_QUANTITY_RULE", $bean, "Quantities greater than threshold value {qty}",{newScore}))