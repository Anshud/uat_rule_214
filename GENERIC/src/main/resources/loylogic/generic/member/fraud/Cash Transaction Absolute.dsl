[when] When the absolute cash transaction exceeds blacklisted limit {amount} for the programs {prgCode} = $bean:FraudManagementBean($score:fraudScore,(programCode in ({prgCode}) && orderTotalCash > {amount}))

[then] Increment Fraud Score by {fraudScore} = $bean.setFraudScore($score + new Integer({fraudScore}))
[then] Set absolute cash transaction individualFraudScores {newScore} and {threshold}= $bean.setIndividualFraudScores(manageFraudScores("CASH_INVOLVED_IN_TRANSACTION_ABSOLUTE", $bean, "Cash component: "+$bean.getOrderTotalCash().toString()+" || Threshold: "+{threshold}, {newScore}))