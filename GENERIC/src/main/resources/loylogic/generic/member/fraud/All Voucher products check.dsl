[when] When the order has any voucher for {programCode}= $bean:FraudManagementBean($score:fraudScore,eval(checkOrderForAllVoucherProducts($bean)),programCode in ({programCode}))

[when] When the order has voucher products for {programCode} and products in {productCodes} = $bean:FraudManagementBean($score:fraudScore,eval(checkOrderForAllVoucherProductCodes($bean,{productCodes})),programCode in ({programCode}))

[when] When shipping country is germany and the order has voucher products for {programCode} and products in {productCodes} = $bean:FraudManagementBean($score:fraudScore,eval(checkOrderForAllVoucherProductCodes(($bean),{productCodes})),programCode in ({programCode}), countryInCart in ("de"))


[then] Increment Fraud Score by {fraudScore} = $bean.setFraudScore($bean.getFraudScore() + new Integer({fraudScore}))

[then] Set individualFraudScores for Voucher Products in ABBS {newScore} = $bean.setIndividualFraudScores(manageFraudScores("VOUCHER_PRODUCTS_IN_ABBS_ORDER", $bean,"",{newScore}))

[then] Set individualFraudScores using Voucher Product Codes in ABBS {newScore} = $bean.setIndividualFraudScores(manageFraudScores("VOUCHER_PRODUCTS_IN_ABBS_ORDER_USING_PRODUCT_CODES", $bean,"",{newScore}))

[then] Set individualFraudScores for Monetary API Products {newScore} = $bean.setIndividualFraudScores(manageFraudScores("MONETARY_API_PRODUCTS_IN_ORDER", $bean,"",{newScore}))

[then] Set individualFraudScores using Voucher Product Codes in MMBS {newScore} = $bean.setIndividualFraudScores(manageFraudScores("PUT_VOUCHER_PRODUCTS_ON_HOLD_IN_MMBS_ORDER", $bean,"",{newScore}))

[then] Set individualFraudScores using Voucher Product Codes in JPREX {newScore} = $bean.setIndividualFraudScores(manageFraudScores("PUT_VOUCHER_PRODUCTS_ON_HOLD_IN_JPREX_ORDER", $bean,"",{newScore}))