[when] When the member Delivery address belongs to the Profile Address group = $bean:FraudManagementBean($score:fraudScore,eval(checkDeliveryAdrsAndProfileAdrs($bean)))

[when] Member Delivery address and Profile Address matching Using Edit Distance with threshold {threshold} excluding {programs} = $bean:FraudManagementBean($score:fraudScore,programCode not in ({programs}), eval(checkDeliveryAndProfileAddressUsingEditDistance($bean,{threshold})))



[then] Increment Fraud Score by {fraudScore} = $bean.setFraudScore($score + new Integer({fraudScore}))
[then] Set delivery address vs profile address individualFraudScores {newScore} = $bean.setIndividualFraudScores(manageFraudScores("DELIVERY_ADDRESS_DIFFERENT_FROM_PROFILE", $bean, ("Delivery address: "+ skipIfNull($bean.getDeliveryAddrLine1(),$bean.getDeliveryAddrLine2(),$bean.getDeliveryAddrCity(),$bean.getDeliveryAddrState(),$bean.getDeliveryAddrZipCode())) +" || "+ ("Profile address: "+ skipIfNull($bean.getAddress(),$bean.getAddress2(),$bean.getCity(), $bean.getStateCode(),$bean.getZipCode())), {newScore}))


[then] Set the delivery address vs profile address individualFraudScores using Edit Distance logic {newScore} = $bean.setIndividualFraudScores(manageFraudScores("DELIVERY_ADDRESS_DIFFERENT_FROM_PROFILE_EDIT_DIST", $bean, ("Delivery address: "+ skipIfNull($bean.getDeliveryAddrLine1(),$bean.getDeliveryAddrLine2(),$bean.getDeliveryAddrCity(),$bean.getDeliveryAddrState(),$bean.getDeliveryAddrZipCode())) +" || "+ ("Profile address: "+ skipIfNull($bean.getAddress(),$bean.getAddress2(),$bean.getCity(), $bean.getStateCode(),$bean.getZipCode())), {newScore}))