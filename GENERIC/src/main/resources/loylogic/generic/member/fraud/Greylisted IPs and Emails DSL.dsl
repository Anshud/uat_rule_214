[when] When the member ip address for ETBS is to be on hold ip group  = $bean:FraudManagementBean( $score:fraudScore, ( programCode in ("ETBS") && eval(checkIPBlacklist(ipAddress, "80.227.67.132,94.58.168.143"))))

[when] from greylisted email group for ETBS = eval(checkEmailBlacklist(deliveryAddrPrimaryEmailId, deliveryAddrSecEmailId, "ulkamxg@gmail.com,bakinkaexe@gmail.com")))


[then] Increment Fraud Score by {fraudScore} = $bean.setFraudScore($score + new Integer({fraudScore}))
[then] Set Ip individualFraudHoldScores {newScore} = $bean.setIndividualFraudScores(manageFraudScores("GREYLIST_IP_ADDRESS", $bean, $bean.getIpAddress(),{newScore}))