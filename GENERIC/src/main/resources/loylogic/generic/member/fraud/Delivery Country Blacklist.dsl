[when] When the member Delivery address country belongs to the blacklisted deliveryCountry group {countries} excluding programs {program} = $bean:FraudManagementBean($score:fraudScore,programCode not in ({program}),eval(checkDeliveryCountryBlacklist(deliveryAddrCountry, "{countries}")))

[when] When the member Delivery address country belongs to the blacklisted deliveryCountry group {countries} including programs {program} = $bean:FraudManagementBean($score:fraudScore,programCode in ({program}),eval(checkDeliveryCountryBlacklist(deliveryAddrCountry, "{countries}")))

[when] When the member Delivery address country belongs to the non blacklisted deliveryCountry group {countries} including programs {program} = $bean:FraudManagementBean($score:fraudScore,programCode in ({program}), eval(checkDeliveryCountryNotBlacklist(deliveryAddrCountry, "{countries}")))

[when] Earnshop Rule when the member Delivery address country belongs to the blacklisted deliveryCountry group {countries} = $bean:FraudManagementBean($score:fraudScore,eval(checkDeliveryCountryBlacklist(deliveryAddrCountry, "{countries}")),programCode in ("ETHACCSHP","HHES","ETES"))


[then] Increment Fraud Score by {fraudScore} = $bean.setFraudScore($score + new Integer({fraudScore}))
[then] Set Delivery Country individualFraudScores {newScore} = $bean.setIndividualFraudScores(manageFraudScores("BLACKLIST_DELIVERY_COUNTRY", $bean, $bean.getDeliveryAddrCountry(),{newScore}))
[then] Set Greylist Delivery Country individualFraudScores {newScore} = $bean.setIndividualFraudScores(manageFraudScores("GREYLIST_DELIVERY_COUNTRY", $bean, $bean.getDeliveryAddrCountry(),{newScore}))