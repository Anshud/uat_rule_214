[when] When the number of rewards with redeemable flag count in the same order exceeds blacklisted limit excluding ETH, ABBS and AKRUU {redeemableFlagCount}= $bean:FraudManagementBean($score:fraudScore, eval(checkRedeemableFlagCount($bean, {redeemableFlagCount})), programCode not in ("ETH","GPTES","ABBS"))

[when] When the number of rewards with redeemable flag count in the same order exceeds blacklisted limit for ETH {redeemableFlagCount}= $bean:FraudManagementBean($score:fraudScore, eval(checkRedeemableFlagCount($bean, {redeemableFlagCount})), programCode in ("ETH"))

[when] When the number of rewards with redeemable flag count in the same order exceeds blacklisted limit for ABBS {redeemableFlagCount}= $bean:FraudManagementBean($score:fraudScore, eval(checkRedeemableFlagCount($bean, {redeemableFlagCount})), programCode in ("ABBS"))


[then] Increment Fraud Score by {fraudScore} = $bean.setFraudScore($score + new Integer({fraudScore}))

[then] Set Order redeemable flag Count individualFraudScores {newScore} and {threshold} = $bean.setIndividualFraudScores(fraudScoresOnRedeemableFlag("MULTIPLE_ONLINE_REDEEMABLE_REWARDS_IN_SAME_ORDER", $bean, {threshold},{newScore}))


