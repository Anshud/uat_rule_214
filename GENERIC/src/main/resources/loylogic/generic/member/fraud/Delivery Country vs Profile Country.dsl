[when] When the member Delivery address country belongs to the Profile Country group = $bean:FraudManagementBean($score:fraudScore,eval(checkDeliveryAndProfileCountries($bean, countryCode)))

[then] Increment Fraud Score by {fraudScore} = $bean.setFraudScore($score + new Integer({fraudScore}))
[then] Set Delivery country vs Profile Country individualFraudScores {newScore} = $bean.setIndividualFraudScores(manageFraudScores("PROFILE_COUNTRY_VS_DELIVERY_COUNTRY", $bean, ("Delivery country: "+ $bean.getDeliveryAddrCountry()+" || "+ "Profile country: "+ $bean.getCountryCode()), {newScore}))