[when] When the consecutive failed ordes count exceeds blacklisted limit {FOCcount} = $bean:FraudManagementBean($score:fraudScore, failedOrderCountConsecutive > {FOCcount})

[when] When the Non-consecutive failed ordes count exceeds blacklisted limit {FONCcount} = $bean:FraudManagementBean($score:fraudScore, failedOrderCountNonConsecutive > {FONCcount})


[then] Increment Fraud Score by {fraudScore} = $bean.setFraudScore($score + new Integer({fraudScore}))
[then] Set Consecutive Failed Orders individualFraudScores {newScore} and {threshold} = $bean.setIndividualFraudScores(manageFraudScores("FAILED_ORDERS_CONSECUTIVE", $bean, "No. of orders: "+$bean.getFailedOrderCountConsecutive().toString()+" || Threshold: "+{threshold},{newScore}))

[then] Increment Fraud Score by {fraudScore} = $bean.setFraudScore($score + new Integer({fraudScore}))
[then] Set Non Consecutive Failed Orders individualFraudScores {newScore} and {threshold1} = $bean.setIndividualFraudScores(manageFraudScores("FAILED_ORDERS_NONCONSECUTIVE", $bean, "No. of orders: "+$bean.getFailedOrderCountNonConsecutive().toString()+" || Threshold: "+{threshold1},{newScore}))