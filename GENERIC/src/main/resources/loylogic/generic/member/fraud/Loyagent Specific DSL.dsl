[when] Do not put orders by Loyagent on Hold for {programCode}= $bean:FraudManagementBean($score:fraudScore, isFromLoyAgent in ("true") ,programCode in ({programCode}))



[then] Increment Fraud Score by {fraudScore} = $bean.setFraudScore($bean.getFraudScore() + new Integer({fraudScore}))

[then] Set individualFraudScores for Loyagent Orders for SC {newScore} = $bean.setIndividualFraudScores(manageFraudScores("LOYAGENT_ORDERS_SHOULD_NOT_BE_ON_HOLD_FOR_SC", $bean,"",{newScore}))
