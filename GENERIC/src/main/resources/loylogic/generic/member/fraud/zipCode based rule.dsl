[when] When the member Delivery address zipcode not match with profile address zipcode = $bean:FraudManagementBean( $score:fraudScore,  eval(compareZipCodes($bean, countryCode)))

[then] Increment Fraud Score by {fraudScore} = $bean.setFraudScore($score + new Integer({fraudScore}))
[then] Set zip code based individualFraudScores {newScore} = $bean.setIndividualFraudScores(manageFraudScores("ZIPCODE_BASED_RULE", $bean, ("Delivery ZIP code: "+$bean.getDeliveryAddrZipCode()+" || "+"Profile ZIP code: "+$bean.getZipCode()),{newScore}))