[when] When the order has annual fee or Statement Credit products for {programCode} do not put order on Hold redemptionSubType inclucing {redemptionSubType} = $bean:FraudManagementBean($score:fraudScore, programCode in ({programCode}), eval(checkSubRedemptionTypes(($bean),{redemptionSubType})))


[then] Increment Fraud Score by {fraudScore} = $bean.setFraudScore($bean.getFraudScore() + new Integer({fraudScore}))

[then] Set individualFraudScores for Annual fees and Statement Credit Orders {newScore} = $bean.setIndividualFraudScores(manageFraudScores("DO_NOT_HOLD_SC_CF_ORDER_RULE", $bean,"",{newScore}))