[when] When instance for the program with field {field} value {value} = $bean:ProgramMemberBean( programCode == "NDGPTREX", eval(checkInstance(($bean), {field}, {value})))


[then] Set the segmentCode of member {segmentCode} = $bean.setSegmentCode({segmentCode});
[then] Apply your settings = update($bean);